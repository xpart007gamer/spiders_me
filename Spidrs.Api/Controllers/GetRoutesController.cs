﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Spidrs.Dto.ViewModels;
using Spidrs.Services.RouteListService;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spiders.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GetRoutesController : ControllerBase
    {
        public IConfiguration _configuration;
        private readonly ILogger<GetRoutesController> _logger;
        private readonly IRouteService _routeService;

        public GetRoutesController(ILogger<GetRoutesController> logger, IConfiguration configuration, IRouteService routeService)
        {
            _logger = logger;
            _configuration = configuration;
            _routeService = routeService;            
        }


        [HttpPost(Name = "GetRoutesByWeek")]
        public async Task<IEnumerable<RouteListApi>> Post(string days)
        {
            var dayNames = days.Split(',');
            //string formattedDays = "";
            //foreach (var day in dayNames)
            //{
            //    if(day != dayNames.Last())
            //    {
            //        formattedDays += "'" + day + "'"+ ",";
            //    }
            //    else
            //    {
            //        formattedDays += "'" + day + "'";
            //    }
               
            //}
            //var query = @"select a.routeID,a.route,a.dayofweek,b.frequency from AllRoutes A inner join AllContainers B on A.dayofweek=B.dayofweek
            //where A.dayofweek in(" + String.Join(",", days) + @") order by A.daynumeric";
            //var query = @"select a.routeID,a.route,a.dayofweek,b.frequency from AllRoutes A inner join AllContainers B on A.dayofweek=B.dayofweek
            //where A.dayofweek in(" + String.Join(",", formattedDays) + @") order by A.daynumeric";

            //var data = await _db.RoutesApi.FromSqlRaw(query).ToListAsync();
            //var data = await _routeService.GetRouteListByApi(formattedDays);
            var data = await _routeService.GetRouteListByApi(dayNames.ToList());
            return data;
        }
    }
}
