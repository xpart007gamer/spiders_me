﻿
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Spiders.Api.Model;
using Spidrs.DataAccess;
using Spidrs.Services.LoginApiService;
//using Spidrs.Utility;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Spiders.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        public IConfiguration _configuration;
        private readonly ILogger<LoginController> _logger;
        public readonly ILoginApiService _loginApiService;
        private readonly SignInManager<IdentityUser> _signInManager;
        public LoginController(ILogger<LoginController> logger, IConfiguration configuration, ApplicationDbContext context, SignInManager<IdentityUser> signInManager, ILoginApiService loginApiService)
        {
            _logger = logger;
            _loginApiService = loginApiService;
            _configuration = configuration;
            _signInManager = signInManager;
        }




        [HttpPost(Name = "GetJwt")]
        public async Task<IActionResult> Post(string UserName,string Password)
        {
           
          
                var userData = await GetUser(UserName, Password);
            if(userData != null)
            {
                var jwt = GenerateJwtToken(UserName, Password);
                var userInfo = new User()
                {
                    UserName = userData.UserName,
                    Email = userData.Email,
                    PhoneNumber = userData.PhoneNumber,
                    JwtToken = jwt,
                };
                return Ok(userInfo);
                
            }


            return NotFound("Unauthorized access");


        }
        

        internal  string GenerateJwtToken(string UserName, string Password)
        {
            if (UserName != null && Password != null)
            {
                var userData = GetUser(UserName, Password);
                var jwt = _configuration.GetSection("Jwt").Get<Jwt>();
                if (userData != null)
                {
                    var claims = new[]
                    {
                        new Claim(JwtRegisteredClaimNames.Sub, jwt.Subject),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
                  
                        new Claim("UserName", UserName),
                        new Claim("Password", Password)

                    };
                    var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwt.key));
                    var signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                    var token = new JwtSecurityToken(
                       jwt.Issuer,
                       jwt.Audience,
                        claims,
                        expires: DateTime.Now.AddMinutes(20),
                        signingCredentials: signIn
                    );
                    return new JwtSecurityTokenHandler().WriteToken(token).ToString();
                }
                else
                {
                    return "";
                }


            }
            else
            {
                return "";
            }
        }

        internal async Task<User?> GetUser(string username, string password)
        {
             var user = await _loginApiService.GetUserByUsername(username);
            if(user != null)
            {
               
                var result = await _signInManager.PasswordSignInAsync(username, password, false, false);
                if (result.Succeeded)
                {
                    var userInfo = new User()
                    {
                        UserName = user.UserName,
                        Email = user.Email,
                        PhoneNumber = user.PhoneNumber,


                    };
                    return userInfo;
                }
            }

            return null;
           
           
                 
        }
    }
}
