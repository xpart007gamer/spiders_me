using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Spidrs.Api;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Spiders.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class SignInController : ControllerBase
    {
       

        private readonly ILogger<SignInController> _logger;

        public SignInController(ILogger<SignInController> logger)
        {
            _logger = logger;
        }

        [HttpGet(Name = "SignIn")]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),               
            })
            .ToArray();            
        }
    }
}