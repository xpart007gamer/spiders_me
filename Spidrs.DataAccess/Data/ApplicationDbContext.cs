﻿
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Spidrs.Dto;
using Spidrs.Dto.Model;
using Spidrs.Dto.ViewModels;

namespace Spidrs.DataAccess
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) 
        {
             
        }

        public DbSet<Driver> Drivers { get; set; }
        public DbSet<RouteList> Routes { get; set; }
        public DbSet<ContainerList> Containers { get; set; }
        public DbSet<RouteListApi> RoutesApi { get; set; }
        public DbSet<AllContainers> AllContainers { get; set; }
        public DbSet<AllRoutes> AllRoutes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RouteList>().HasNoKey();
            modelBuilder.Entity<ContainerList>().HasNoKey();
            modelBuilder.Entity<RouteListApi>().HasNoKey();
            base.OnModelCreating(modelBuilder);
            //modelBuilder.Entity<IdentityUserLogin<string>>().HasNoKey();
            //modelBuilder.Entity<IdentityUserRole<string>>().HasNoKey();
            //modelBuilder.Entity<IdentityUserToken<string>>().HasNoKey();
        }
    }
}
