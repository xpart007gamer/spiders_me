﻿using Spidrs.DataAccess.Repository.IRepository;
using Spidrs.Dto;

namespace Spidrs.DataAccess.Repository
{
    public class ApplicationRepository : Repository<ApplicationUser>, IApplicationRepository
    {
        private ApplicationDbContext _db;

        public ApplicationRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
      
    
    }
}
