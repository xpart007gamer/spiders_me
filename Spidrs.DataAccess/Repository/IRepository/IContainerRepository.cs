﻿using Spidrs.Dto.ViewModels;
using System.Collections.Generic;

namespace Spidrs.DataAccess.Repository.IRepository
{
    public interface IContainerRepository
    {
        public List<ContainerList> ContainerList(int start, string searchvalue, int Length, string SortColumn, string sortDirection);
    }
}
