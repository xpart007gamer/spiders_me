﻿using Spidrs.Dto.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spidrs.DataAccess.Repository.IRepository
{
    public interface IRoutesRepository : IRepository<RouteList>
    {
         public List<RouteList> RouteList(int start, string searchvalue, int Length, string SortColumn, string sortDirection);
    }
}
