﻿using Spidrs.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spidrs.DataAccess.Repository.IRepository
{
    public interface IUserRepository : IRepository<Driver>
    {
        void Update(Driver driver);
    }
}
