﻿using Spidrs.DataAccess.Repository.IRepository;
using Spidrs.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spidrs.DataAccess.Repository
{
    public class UserRepository : Repository<Driver>, IUserRepository
    {
        private ApplicationDbContext _db;

        public UserRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        public void Update(Driver driver)
        {
            _db.Drivers.Update(driver);
        }
    }
}
