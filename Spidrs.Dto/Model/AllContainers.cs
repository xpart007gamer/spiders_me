﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spidrs.Dto.Model
{
    [Keyless]
    public class AllContainers
    {
        public int containerid { get; set; }
        public int customerid { get; set; }
        public string backofficecustomerid { get; set; }
        public string accountname { get; set; }
        public string serviceaddress { get; set; }
        public string container { get; set; }
        public string frequency { get; set; }
        public string route { get; set; }
        public int? sortorder { get; set; }
        public int? size { get; set; }
        public string dayofweek { get; set; }
        public DateTime? maintained { get; set; }
        public int? relativeorder { get; set; }
        public int? totalcontainers { get; set; }
        public string seasonalcode { get; set; }
        public string routenote { get; set; }
        public string drivernote { get; set; }
        public bool? haslock { get; set; }
        public string previousnumber { get; set; }
        public bool? association { get; set; }
        public string modifiedby { get; set; }
        public DateTime? modifieddate { get; set; }
        public DateTime? createddate { get; set; }
    }
}
