﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spidrs.Dto.Model
{
    [Keyless]
    public class AllRoutes
    {
        public int? routeID { get; set; }

        public string route { get; set; }

        public int? daynumeric { get; set; }

        public string dayofweek { get; set; }

        public string modifiedby { get; set; }

        public DateTime? modifieddate { get; set; }

        public DateTime? createddate { get; set; }

    }
}
