﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spidrs.Dto.ViewModels
{
    public class ContainerList
    {
        public int containerid { get; set; }
        public string container { get; set; }
        public string accountname { get; set; }
        public string dayofweek { get; set; }

        public DateTime maintained { get; set; }
        public string backofficecustomerid { get; set; }
        public string serviceaddress { get; set; }
        public string frequency { get; set; }
        public int size { get; set; }
        public string drivernote { get; set; }

        public int TotalRecords { get; set; }
    }
}
