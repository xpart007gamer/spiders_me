﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spidrs.Dto.ViewModels
{
    public class RouteListApi
    {
        public int routeID { get; set; }
        public int daynumeric { get; set; }
        public string route { get; set; }
        public string dayofweek { get; set; }
        public string frequency { get; set; }
       
    }
}
