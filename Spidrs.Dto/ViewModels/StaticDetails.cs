﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spidrs.Dto.ViewModels
{
    public static class StaticDetails
    {
        public const string Role_User = "Driver";
        public const string Role_Admin = "Admin";

    }
}
