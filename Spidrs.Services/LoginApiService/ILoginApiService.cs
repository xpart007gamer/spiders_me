﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spidrs.Services.LoginApiService
{
    public interface ILoginApiService
    {
        Task<IdentityUser> GetUserByUsername(string username);
    }
}
