﻿using Microsoft.AspNetCore.Identity;
using Spidrs.DataAccess;
using Spidrs.Dto.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spidrs.Services.LoginApiService
{
    public class LoginApiService : ILoginApiService
    {
        public readonly ApplicationDbContext _context;
        public LoginApiService(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IdentityUser> GetUserByUsername(string username)
        {
            var user = _context.Users.FirstOrDefault(u => u.UserName == username);
            return user;
        }
       
    }
}
