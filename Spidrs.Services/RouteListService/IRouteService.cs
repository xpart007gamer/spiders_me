﻿using Spidrs.Dto.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spidrs.Services.RouteListService
{
    public interface IRouteService
    {
        public List<RouteList> RouteList(int start, string searchvalue, int Length, string SortColumn, string sortDirection);
        //Task<IEnumerable<RouteListApi>> GetRouteListByApi(string days);
        Task<IEnumerable<RouteListApi>> GetRouteListByApi(List<string> days);
    }
}
