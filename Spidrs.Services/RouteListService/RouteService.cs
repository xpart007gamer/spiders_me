﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Spidrs.DataAccess;
using Spidrs.Dto.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spidrs.Services.RouteListService
{
    public class RouteService : IRouteService
    {
        private readonly ApplicationDbContext _db;
        public RouteService(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<IEnumerable<RouteListApi>> GetRouteListByApi(List<string> days) //string days
        {
            var result = await (from route in _db.AllRoutes.AsNoTracking().Where(x => days.Contains(x.dayofweek.ToLower()))
                          join container in _db.AllContainers.AsNoTracking() on route.dayofweek equals container.dayofweek 
                          orderby route.daynumeric
                          select new RouteListApi
                          {
                              routeID = route.routeID.Value,
                              daynumeric  = route.daynumeric.Value,
                              dayofweek = route.dayofweek,
                              frequency = container.frequency,
                              route = route.route
                          }).ToListAsync();
            return result;
            //var query = @"select a.routeID,a.route,a.daynumeric,a.dayofweek,b.frequency from AllRoutes A inner join AllContainers B on A.dayofweek=B.dayofweek
            //where A.dayofweek in(" + String.Join(",", days) + @") order by A.daynumeric";

            //var data = await _db.RoutesApi.FromSqlRaw(query).ToListAsync();
            //return data;
        }

        public List<RouteList> RouteList(int start, string searchvalue, int Length, string SortColumn, string sortDirection)
        {

            SqlParameter usernameParam1 = new SqlParameter("@Pageno", start);
            SqlParameter usernameParam2 = new SqlParameter("@filter", searchvalue);
            SqlParameter usernameParam3 = new SqlParameter("@pagesize", Length);
            SqlParameter usernameParam4 = new SqlParameter("@Sorting ", SortColumn);
            SqlParameter usernameParam5 = new SqlParameter("@SortOrder", sortDirection);
            var routes = _db.Routes
            .FromSqlRaw("exec GetRoutes @Pageno, @filter,@pagesize,@Sorting,@SortOrder", usernameParam1, usernameParam2, usernameParam3, usernameParam4, usernameParam5)
            .ToList();
            return routes;
        }
    }
}
