﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.V4.Pages.Account.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using Spidrs.DataAccess;
using Spidrs.DataAccess.Repository.IRepository;
using Spidrs.Dto.ViewModels;
using Spidrs.Dto;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using System.Threading;
using System;

namespace Spidrs.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class UserController : Controller
    {

        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IUserStore<IdentityUser> _userStore;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<RegisterModel> _logger;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ApplicationDbContext _context;


        public UserController(
           UserManager<IdentityUser> userManager,
           IUserStore<IdentityUser> userStore,
           SignInManager<IdentityUser> signInManager,
           ILogger<RegisterModel> logger,
         IUnitOfWork unitOfWork,
         ApplicationDbContext context,
           RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _userStore = userStore;
            _unitOfWork = unitOfWork;
            _signInManager = signInManager;
            _logger = logger;
            _context = context;
            _roleManager = roleManager;

        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Add()
        {

            var roles = _roleManager.Roles.Where(r => r.Name != "Admin");


            var User = new DriverVm
            {
                RoleList = roles.Select(s => new SelectListItem
                {
                    Text = s.Name,
                    Value = s.Name
                })
            };
            return View(User);
        }

        public IActionResult Edit(int Id)
        {


            var roles = _roleManager.Roles.Where(r => r.Name != "Admin");
            var data = _unitOfWork.User.GetFirstOrDefault(f => f.Id == Id);
            var driver = new DriverVmEdit();
            driver.Id = data.Id;
            driver.FirstName = data.FirstName;
            driver.LastName = data.LastName;
            driver.Email = data.Email;
            driver.StreetAddress = data.StreetAddress;
            driver.PhoneNumber = data.PhoneNumber;
            driver.City = data.City;
            driver.PostalCode = data.PostalCode;
            driver.Role = data.Role;
            driver.Password = data.Password;
            driver.ConfirmPassword = data.ConfirmPassword;
            driver.RoleList = roles.Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Name
            });
            return View(driver);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Add(DriverVm model)
        {


            if (ModelState.IsValid)
            {

                var user = CreateUser();

                await _userStore.SetUserNameAsync(user, model.Email, CancellationToken.None);

                user.Name = model.FirstName + " " + model.LastName;
                user.Email = model.Email;
                user.State = model.State;
                user.StreetAddress = model.StreetAddress;
                user.City = model.City;
                user.PostalCode = model.PostalCode;
                user.EmailConfirmed = true;
                var result = await _userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    _logger.LogInformation("User created a new account with password.");
                    if (model.Role == null)
                    {
                        await _userManager.AddToRoleAsync(user, StaticDetails.Role_User);
                    }
                    else
                    {
                        await _userManager.AddToRoleAsync(user, model.Role);
                    }

                    var driver = new Driver();
                    driver.Id = 0;
                    driver.FirstName = model.FirstName;
                    driver.LastName = model.LastName;
                    driver.Email = model.Email;
                    driver.Role = model.Role != null ? model.Role : "Driver";
                    driver.City = model.City;
                    driver.PostalCode = model.PostalCode;
                    driver.PhoneNumber = model.PhoneNumber;
                    driver.Password = model.Password;
                    driver.ConfirmPassword = model.ConfirmPassword;
                    _unitOfWork.User.Add(driver);
                    _unitOfWork.Save();

                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }


            }

            return Redirect("/Admin/User/Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(DriverVmEdit model)
        {


            if (ModelState.IsValid)
            {
                var data = _unitOfWork.User.GetFirstOrDefault(f => f.Id == model.Id);
                var driver = new Driver();
                data.Id = model.Id;
                data.FirstName = model.FirstName;
                data.LastName = model.LastName;
                data.Email = model.Email;
                data.Role = model.Role != null ? model.Role : "Driver";
                data.City = model.City;
                data.PostalCode = model.PostalCode;
                data.StreetAddress = model.StreetAddress;
                data.PhoneNumber = model.PhoneNumber;

                _unitOfWork.User.Update(data);
                _unitOfWork.Save();


            }

            return Redirect("/Admin/User/Index");
        }


        [HttpDelete]
        public IActionResult Delete(int? id)
        {

            var driver = _unitOfWork.User.GetFirstOrDefault(f => f.Id == id);
            if (driver == null)
            {
                return Json(new { success = false, message = "Error while Deleting" });
            }

            _unitOfWork.User.Remove(driver);
            _unitOfWork.Save();
            return Json(new { success = true, message = "Deleted Successfully!" });
        }

        [HttpGet]
        public IActionResult GetUsers()
        {

            var driverList = _unitOfWork.User.GetAll();
            return Json(new { data = driverList, recordsTotal = driverList.Count(), recordsFiltered = driverList.Count() });
        }


        private ApplicationUser CreateUser()
        {
            try
            {
                return Activator.CreateInstance<ApplicationUser>();
            }
            catch
            {
                throw new InvalidOperationException($"Can't create an instance of '{nameof(IdentityUser)}'. " +
                    $"Ensure that '{nameof(IdentityUser)}' is not an abstract class and has a parameterless constructor, or alternatively " +
                    $"override the register page in /Areas/Identity/Pages/Account/Register.cshtml");
            }
        }

    }
}
