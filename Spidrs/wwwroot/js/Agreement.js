﻿$(document).ready(function () {


    var tblcustomer = $('#tblAgreementList').DataTable({


        language: {
            "search": '<i class="fa fa-search"></i>',
            searchPlaceholder: "Search records",
            paginate: {
                next: '<i class="fa fa-angle-right"></i>',
                previous: '<i class="fa fa-angle-left"></i>'
            }
        },
        "searching": true,
        "lengthChange": false,
        "paging": true,
        "info": true,
        "order": []

    });
  
});

//Add dyanamic row
$(document).on('click', '#btnAddNewRow', function () {
    debugger
    var firstRow = $(this).closest('table').find('tr').eq(1);
    $('#hdnHtml').html('');
    $('#hdnHtml').html($(firstRow).html());
    $('#hdnHtml').html($('#hdnHtml').html().replace('AgreementType', 'AgreementType' + makeid(5) + ''));
    $('#hdnHtml').html($('#hdnHtml').html().replace('fileupload', 'fileupload' + makeid(5) + ''));
    $('#hdnHtml').html($('#hdnHtml').html().replace('btnDeleteNewRow', 'btnDeleteNewRow'));
    $('#btnDeleteNewRow').css('display', 'block');
    _dvPreviewId = makeid(5);
    $('#hdnHtml').html($('#hdnHtml').html().replace('dvPreview', 'dvPreview' + _dvPreviewId + ''));
    $('#dvPreview' + _dvPreviewId + '').children().remove();
    $('#hdnHtml').html($('#hdnHtml').html().replace('errmsg1', 'errmsg1' + makeid(5) + ''));
    $('#hdnHtml').html($('#hdnHtml').html().replace('errwebsite', 'errwebsite' + makeid(5) + ''));
    $(this).closest('tr').after('<tr>' + $('#hdnHtml').html() + '</tr>');
    $('#hdnHtml').html('');
});


//Generate rendom number
function makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
///Save the Multiple data images and websiteurl
function Saveall() {
    debugger
    var AgreementType = "";
    var file = "";
    var formData1 = new FormData();
    formData1.append("ID", 1);
    $("#Advertismenttable TBODY TR").each(function (index, value) {
        var row = $(this);
        file = row.find(".fileclass")[0].files[0];
        AgreementType = row.find(".AgreementType").val()
        var errorimg = row.find(".errorimg").attr('id')
        var errorweb = row.find(".errorweb").attr('id')
        if (AgreementType == "") {
            $("#" + errorweb).html("Please enter agreement type").show()
            event.preventDefault()
        }
        else if (file == "" || file == undefined) {
            event.stopPropagation();
            $("#" + errorimg).html("Please select agreement").show()
            event.preventDefault()
        }

        else {
            formData1.append(row.find(".fileclass")[0].files[0].name, row.find(".fileclass")[0].files[0]);
            formData1.append("AgreementType", row.find(".AgreementType").val());
            formData1.append("ordernums", row.find(".ordernumbers").val());
        }


    });
    if (AgreementType != "" && file != "" && file != undefined) {
        var url = $("#hdSaveMultipleAgreement").val();
        $.ajax({
            type: "Post",
            url: url,
            contentType: false,
            processData: false,
            data: formData1,
            success: function (result) {
                if (result == 1) {
                    $("div.success").show();
                    $("div.success").fadeIn(400).delay(1600).fadeOut(3000);
                    setTimeout(function () {
                        var url = $("#hdnAgreement").val();
                        window.location.href = url;
                    }, 300);
                }

                //else {

                //    $("div.failure").show();
                //    $("div.failure").fadeIn(400).delay(1600).fadeOut(3000);
                //}
            },
            error: function (response) {

            }
        });
    }
}

function DeletePOP(ID) {

    $('#agreementid').val(ID);
    $('#myModal45656').modal('show');
}
function Delete() {
    var id = $('#agreementid').val();
        alert(id);
}
function Delete() {
    debugger
    var id = $('#agreementid').val();
 
    var url = $('#hdnDeleteAgreement').val();
    $.ajax({
        type: "Post",
        url: url,
        data: { id: id},
        success: function (result) {
            if (result == true) {
                $("#myModal45656").modal("hide")
                $("div.deletemsgfailure").show();
                $("div.deletemsgfailure").fadeIn(300).delay(1500).fadeOut(4000);
                setTimeout(function () {
                    var url = $("#hdnAgreement").val();
                    window.location.href = url;
                }, 3000);
            }
            else {
                $("#MyModalMail").modal("hide")
                $("div.failure").show();
                $("div.failure").fadeIn(300).delay(1500).fadeOut(4000);
                setTimeout(function () {
                    var url = $("#hdnAgreement").val();
                    window.location.href = url;
                }, 3000);
            }
        },
        error: function (response) {
        }
    });
}
$(document).on('click', '#btnDeleteNewRow', function () {
    var rowId = $(this).closest('tr');
    rowId.remove();
});


///validation check
$('#checkvalidation').click(function () {

    var WebsitePath = $('#WebsitePath').val();
    if (WebsitePath == "") {
        $("#errmsg").html("Please Select Website Path").show().fadeOut(3000);
        return false;
    }
    var ID = $('#ID').val();
    if (ID == 0 || ID == "" || ID == undefined) {
        var file1 = $("#fileupload")[0].files[0]

        if (file1 == "" || file1 == undefined) {
            $("#errmsg1").html("Please Select Image").show().fadeOut(3000);
            return false;
        }
    }
});

////Check the image duplicate
function Checkduplicateimage() {

    var file1 = $("#" + fileupload + "")[0].files[0]
    if (file1 == "" || file1 == undefined) {
        $("#" + errormsg + "").html("Please Select Image").show().fadeOut(3000);
        return false;
    }
    var formData = new FormData();
    formData.append("checkimage", file1);
    var url = $('#hdnCheckexistimage').val();
    $.ajax({
        type: "Post",
        url: url,
        contentType: false,
        processData: false,
        data: formData,
        success: function (result) {
            if (result > 0) {
                $("#" + errormsg + "").html("This image already exist.").show();
                $("#" + fileupload + "").val('');
                $("#" + fileupload + "").text('');
                $("#" + imageshowdiv + "").html("");
                return false;
            }
        }
    });
}







////Check the image duplicate /////Update case Advertismnet
function CheckduplicateimageU() {
    var file1 = $("#fileupload1222")[0].files[0]
    if (file1 == "" || file1 == undefined) {
        $("#errmsg121").html("Please Select Image").show().fadeOut(3000);
        return false;
    }
    var formData = new FormData();
    formData.append("checkimage", file1);
    var url = $('#hdnCheckexistimage').val();
    $.ajax({
        type: "Post",
        url: url,
        contentType: false,
        processData: false,
        data: formData,
        success: function (result) {
            if (result > 0) {
                $("#errmsg121").html("This image already exist.").show();
                $("#fileupload1222").val('');
                $("#fileupload1222").text('');
                $("#dvPreview12").html("");
                return false;
            }
        }
    });
}

