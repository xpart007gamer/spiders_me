﻿
var AllCheckedRowsIds = new Array();

///Sent page js start
$(document).ready(function () {
    $("#message").Editor();
    $("#Messages").Editor();
    $("#replymsg").Editor();
    $("#ddlskip").hide();
    $("#fromlibaryr").hide();
    $("#btnfromlibaryr").hide();


    var tblcustomer = $('#tblCommunication').DataTable({

        language: {
            "search": '<i class="fa fa-search"></i>',
            searchPlaceholder: "Search records",
            paginate: {
                next: '<i class="fa fa-angle-right"></i>',
                previous: '<i class="fa fa-angle-left"></i>'
            }
        },
        "searching": true,
        "lengthChange": false,
        "paging": true,
        "info": true,
        "order": []

    });
    //$('#tblAdvanceSearch').DataTable({


    //    "searching": true,
    //    "lengthChange": false,
    //    "paging": true,
    //    "info": true,
    //    "order": []

    //});
    $(".loader-wrapper").removeClass("show1");
    rowCollection = [];
    // var table=  $("#tblCommunication TBODY").closest('tr')
    var id = $("#tblCommunication tbody tr:first").find('.chkRow').val();
    // $("#tblCommunication tbody tr:first").find('.chkRow').prop('checked', true);
    $("#hdcustomerid").val(id);
    var UrlShowamessageDetails = $('#hdShowamessageDetails').val();
    //var rowIndex = this.parentElement.parentElement.rowIndex;
    //if (rowCollection.indexOf(rowIndex) > -1) {
    //    return false;
    //}
    $.ajax({
        type: "Post",
        url: UrlShowamessageDetails,
        data: { id: id },
        success: function (result) {

            $('#subjectdata').html('');
            $('#subjectdata1').html('');
            if (result == 0) {
                $('#openre').css("visibility", "hidden");
                $('#reload').css("visibility", "hidden");
                // $('#inboxhedaer').css("visibility", "hidden");
            }
            else {
                var row = '';
                $.each(result, function (key, value) {

                    if (value != null) {
                        row += `<div class="row mt-3">
                                    <div class="col-md-12 pt-left">
                                        
                                        Sent: <span class="bold" id="subjectdispaly">`+ value.senttodate + `</span><br>
                                        From: <span class="bold" id="subjectdispaly">`+ value.adminEmail + `</span><br>
                                        To: <span class="bold" id="subjectdispaly">`+ value.customerEmail + `</span><br>
                                        Subject: <span class="bold" id="subjectdispaly">`+ value.subject + `</span>
                                    </div>
                                    <div class="col-md-6 text-right f12 pt-left">
                                        <span class="text-primary bold" id="fulldate" style="display:none">`+ value.createdDate1 + `</span>
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-md-12">
                                        <p class="text-justify" id="fullmsg" style="padding-left:15px;">`+ value.messages + `</p>
                                    </div>
                                </div>`
                    }

                });
                //$('table#tblCommunication > tbody > tr').eq(--rowIndex).after(row);
                $('#subjectdata').append(row);
                //$('#subjectdata1').append(row);
                $('#openre').css("visibility", "visible");
                $('#reload').css("visibility", "visible");
                $('#inboxhedaer').css("visibility", "visible");
                //$('#replysubject').val('Re:' + result.subject);
                //$('#hdcustomerid').val(result.messageId);
            }
        }
    })



    //OpenReplyBox();
    //$("#chkAll").click(function () {

    //    //Determine the reference CheckBox in Header row.
    //    var chkAll = this;

    //    //Fetch all row CheckBoxes in the Table.
    //    var chkRows = $("#tblCommunication").find(".chkRow");

    //    //Execute loop over the CheckBoxes and check and uncheck based on
    //    //checked status of Header row CheckBox.
    //    chkRows.each(function () {
    //        $(this)[0].checked = chkAll.checked;
    //    });
    //});

    //$(".chkRow").click(function () {

    //    //Determine the reference CheckBox in Header row.
    //    var chkAll = $("#chkAll");

    //    //By default set to Checked.
    //    chkAll.prop("checked", true);

    //    //Fetch all row CheckBoxes in the Table.
    //    var chkRows = $("#tblCommunication").find(".chkRow");

    //    //Execute loop over the CheckBoxes and if even one CheckBox 
    //    //is unchecked then Uncheck the Header CheckBox.
    //    chkRows.each(function () {
    //        if (!$(this).is(":checked")) {
    //            //chkAll.removeAttr("checked", "checked");
    //            chkAll.prop("checked", false);
    //            return;
    //        }
    //    });
    //});
    if ($("#hdSentTo").val() == 'super admin' || $("#hdSentTo").val() == 'BackOffice User') {
        var dataTable = $("#tblCommunicationSent").dataTable({
            "serverSide": true,
            "order": [[0, "desc"]],
            language: {
                "search": '<i class="fa fa-search"></i>',
                searchPlaceholder: "Search records",
                paginate: {
                    next: 'Next &raquo;',
                    previous: '&laquo; Previous'
                },
                "processing": '<div class="loader-wrapper show1"><div class= "loader" ></div></div>'
            },
            "search": {
                "search": $("#SearchParam").val()
            },
            "processing": true,
            "pagingType": "input",
            "filter": true,
            "orderMulti": false,
            "destroy": true,
            "lengthChange": false,
            "retrieve": true,
            "ajax": {
                url: '/Communication/SentItemList?createdDate=' + $("#DateParam").val() + '&subject=' + $("#SubjectParam").val(),
                method: "POST"
            },
            "drawCallback": function (settings) {
            },

            "columns": [


                {
                    "data": "createdDate", "name": "createdDate", mRender: function (data, type, full) {
                        var date = new Date(full.createdDate);
                        var day = date.getDate();
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();
                        var hour = date.getHours();
                        var min = date.getMinutes();
                        var sec = date.getSeconds();
                        var format = "AM";
                        if (day < 10) {
                            day = "0" + day;
                        }
                        if (month < 10) {
                            month = "0" + month;
                        }
                        if (sec < 10) {
                            sec = "0" + sec;
                        }
                        if (hour > 11) { format = "PM"; }
                        if (hour > 12) { hour = hour - 12; }
                        if (hour == 0) { hour = 12; }
                        if (min < 10) { min = "0" + min; }
                        var date = month + "/" + day + "/" + year + " " + hour + ":" + min + ":" + sec + " " + format;
                        return date;
                    }
                },
                { "data": "company", "name": "company" },
                { "data": "serviceTypeName", "name": "serviceTypeName" },
                {
                    "data": null,
                    "render": function (data, type, full) {
                        return full['lastName'] + ', ' + full['firstName'];
                    }
                },
                { "data": "phone1", "name": "phone1" },
                {
                    "data": "subject", "name": "subject", mRender: function (data, type, full) {
                        return '<label class="getMsgId" onclick="allSentmsg(' + full.messageId + ')" data-msgid="' + full.messageId + '"  data-toggle="collapse" data-target="#rowAppend_' + full.messageId + '" title="After click on Subject line, you can see the conversation" style="cursor:pointer;">' + data + '</label>';
                    }
                }
                ,

                { "data": "initial", "name": "initial" },
                {
                    "data": "fileupload", "name": "fileupload", mRender: function (data, type, full) {
                        if (data) {
                            return '<span><a href="/Communication/DownloadFile/?filename=' + data + '" target="_blank"><i class="fa fa-download" style="color:#337ab7"></i></a></span>';
                        }
                        else {
                            return '<span>No Attachment</span>';
                        }
                    }
                },
                {

                    "data": null,
                    'render': function (data, type, full, meta) {
                        return ' <input class="chkRow singleCheck dt-checkboxes" type="checkbox" data-msgid="' + full.messageId + '">';
                    }
                },
                {
                    "data": "neverExpireByAdmin", "name": "neverExpireByAdmin", mRender: function (data, type, full) {
                        if (full.neverExpireByAdmin == true || full.neverExpireByCustomer == true) {
                            return '<input class="chkRowAdminClose" id="' + full.messageId + '" value="' + full.messageId + '" onclick="return AddForNeverExpireuncheck(this,' + full.messageId + ')" type="checkbox" checked>';
                        }
                        else {
                            return '<input class="chkRowAdminClose" id="' + full.messageId + '" value="' + full.messageId + '" onclick="return AddForNeverExpirechecked(this,' + full.messageId + ')" type="checkbox">';
                        }
                    }
                }
            ],
            'columnDefs': [
                {
                    "targets": 8,
                    "orderable": false,

                }
            ],
            'select': {
                'style': 'multi'
            },
            fnDrawCallback: function () {
                var isCheckedAll = $('#example-select-all').is(':checked');
                if (isCheckedAll) {
                    var chkRows = $("#tblCommunicationSent").find(".singleCheck");
                    chkRows.each(function () {

                        $(this)[0].checked = isCheckedAll;
                        var item = $(this).attr("data-msgid");
                        var index = AllCheckedRowsIds.indexOf(item);

                        if (index == -1) {
                            AllCheckedRowsIds.push($(this).attr("data-msgid"));
                        }

                    });
                }
                else {
                    var chkRows = $("#tblCommunicationSent").find(".singleCheck");
                    chkRows.each(function () {

                        var thisId = $(this).attr("data-msgid");
                        for (var i = 0; i < AllCheckedRowsIds.length; i++) {
                            if (thisId == AllCheckedRowsIds[i]) {
                                $(this).prop("checked", true);
                            }
                        }

                    });
                }
            },
        });
        /*  $(".dt-checkboxes-select-all input[type='checkbox']").click();*/

        $('#example-select-all').on('click', function () {

            var chkAll = this;
            // Get all rows with search applied
            var chkRows = $("#tblCommunicationSent").find(".singleCheck");
            // Check/uncheck checkboxes for all rows in the table
            chkRows.each(function () {

                $(this)[0].checked = chkAll.checked;
                if (chkAll.checked) {
                    var item = $(this).attr("data-msgid");
                    var index = AllCheckedRowsIds.indexOf(item);

                    if (index == -1) {
                        AllCheckedRowsIds.push($(this).attr("data-msgid"));
                    }

                }
                else {
                    /*  removeArrayItem(AllCheckedRowsIds, $(this).attr("data-msgid"));*/
                    var item = $(this).attr("data-msgid")
                    var index = AllCheckedRowsIds.indexOf(item);


                    if (index != -1) {

                        AllCheckedRowsIds.splice(index, 1);
                    }
                    //AllCheckedRowsIds.pop($(this).attr("data-msgid"));
                }

            });
            //$('#tblCommunicationSent input[type="checkbox"]', chkRows).prop('checked', this.checked);
        });



        $(document).on('click', '.singleCheck', function () {

            if (!$(this).is(":checked")) {
                var item = $(this).attr("data-msgid")
                var index = AllCheckedRowsIds.indexOf(item);

                if (index != -1) {

                    AllCheckedRowsIds.splice(index, 1);
                }
                //removeArrayItem(AllCheckedRowsIds, $(this).attr("data-msgid"));

            } else {
                var index = AllCheckedRowsIds.indexOf(item);

                if (index == -1) {
                    AllCheckedRowsIds.push($(this).attr("data-msgid"));
                }
            }
            //Determine the reference CheckBox in Header row.
            var chkAll = $("#example-select-all");



            //By default set to Checked.
            chkAll.prop("checked", true);

            //Fetch all row CheckBoxes in the Table.
            var chkRows = $("#tblCommunicationSent").find(".singleCheck");

            //Execute loop over the CheckBoxes and if even one CheckBox 
            //is unchecked then Uncheck the Header CheckBox.
            chkRows.each(function () {
                if (!$(this).is(":checked")) {

                    //chkAll.removeAttr("checked", "checked");
                    chkAll.prop("checked", false);

                    return;
                }
            });
        });

        $("#tblCommunicationSentMessage").dataTable({
            "serverSide": true,
            "order": [[0, "desc"]],
            language: {
                "search": '<i class="fa fa-search"></i>',
                searchPlaceholder: "Search records",
                paginate: {
                    next: 'Next &raquo;',
                    previous: '&laquo; Previous'
                },
                "processing": '<div class="loader-wrapper show1"><div class= "loader" ></div></div>'
            },
            "processing": true,
            "pagingType": "input",
            "filter": true,
            "orderMulti": false,
            "destroy": true,
            "lengthChange": false,
            "retrieve": true,
            "ajax": {
                url: '/Communication/AdminSentMessage',
                method: "POST"
            },
            "drawCallback": function (settings) {
            },

            "columns": [
                {
                    "data": "createdDate", "name": "createdDate", mRender: function (data, type, full) {
                        var date = new Date(full.createdDate);
                        var day = date.getDate();
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();
                        var hour = date.getHours();
                        var min = date.getMinutes();
                        var sec = date.getSeconds();
                        var format = "AM";
                        if (day < 10) {
                            day = "0" + day;
                        }
                        if (month < 10) {
                            month = "0" + month;
                        }
                        if (sec < 10) {
                            sec = "0" + sec;
                        }
                        if (hour > 11) { format = "PM"; }
                        if (hour > 12) { hour = hour - 12; }
                        if (hour == 0) { hour = 12; }
                        if (min < 10) { min = "0" + min; }
                        //var date = month + "/" + day + "/" + year + " " + hour + ":" + min + ":" + sec + " " + format;
                        var date = month + "/" + day + "/" + year;
                        return date;
                    }
                },
                {
                    "data": "subject", "name": "subject", mRender: function (data, type, full) {
                        return '<label>' + data + '</label>';
                    }
                },
                
                {
                    "data": "createdDate",
                    'render': function (data, type, full, meta) {
                        var date = new Date(full.createdDate);
                        var day = date.getDate();
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();
                        var hour = date.getHours();
                        var min = date.getMinutes();
                        var sec = date.getSeconds();
                        var format = "AM";
                        if (day < 10) {
                            day = "0" + day;
                        }
                        if (month < 10) {
                            month = "0" + month;
                        }
                        if (sec < 10) {
                            sec = "0" + sec;
                        }
                        if (hour > 11) { format = "PM"; }
                        if (hour > 12) { hour = hour - 12; }
                        if (hour == 0) { hour = 12; }
                        if (min < 10) { min = "0" + min; }
                        var date = year + "-" + month + "-" + day;
                        var searchFilter = $(".form-control-sm").val();
                        var searchFilterCheck = "";
                        if (searchFilter != "") {
                            searchFilterCheck = '&searchFilter=' + $(".form-control-sm").val()
                        }
                        return '<a href="/Communication/SentItem?createdDate=' + date + '&subject=' + full.subject + searchFilterCheck + '">View Customers</a>';
                    }
                },
                {
                    "data": "customerCount", "name": "customerCount", mRender: function (data, type, full) {
                        return '<label>' + data + '</label>';
                    }
                }
            ]
        });

    }
    else {
        $('#example-select-all').on('click', function () {

            var chkAll = this;
            // Get all rows with search applied
            var chkRows = $("#tblCommunicationSent").find(".singleCheck");
            // Check/uncheck checkboxes for all rows in the table
            chkRows.each(function () {

                $(this)[0].checked = chkAll.checked;
                if (chkAll.checked) {
                    var item = $(this).attr("data-msgid");
                    var index = AllCheckedRowsIds.indexOf(item);

                    if (index == -1) {
                        AllCheckedRowsIds.push($(this).attr("data-msgid"));
                    }

                }
                else {
                    /*  removeArrayItem(AllCheckedRowsIds, $(this).attr("data-msgid"));*/
                    var item = $(this).attr("data-msgid")
                    var index = AllCheckedRowsIds.indexOf(item);


                    if (index != -1) {

                        AllCheckedRowsIds.splice(index, 1);
                    }
                    //AllCheckedRowsIds.pop($(this).attr("data-msgid"));
                }

            });
            //$('#tblCommunicationSent input[type="checkbox"]', chkRows).prop('checked', this.checked);
        });



        $(document).on('click', '.singleCheck', function () {

            if (!$(this).is(":checked")) {
                var item = $(this).attr("data-msgid")
                var index = AllCheckedRowsIds.indexOf(item);

                if (index != -1) {

                    AllCheckedRowsIds.splice(index, 1);
                }
                //removeArrayItem(AllCheckedRowsIds, $(this).attr("data-msgid"));

            } else {
                var index = AllCheckedRowsIds.indexOf(item);

                if (index == -1) {
                    AllCheckedRowsIds.push($(this).attr("data-msgid"));
                }
            }
            //Determine the reference CheckBox in Header row.
            var chkAll = $("#example-select-all");



            //By default set to Checked.
            chkAll.prop("checked", true);

            //Fetch all row CheckBoxes in the Table.
            var chkRows = $("#tblCommunicationSent").find(".singleCheck");

            //Execute loop over the CheckBoxes and if even one CheckBox 
            //is unchecked then Uncheck the Header CheckBox.
            chkRows.each(function () {
                if (!$(this).is(":checked")) {

                    //chkAll.removeAttr("checked", "checked");
                    chkAll.prop("checked", false);

                    return;
                }
            });
        });
    }




});
function removeArrayItem(array, item) {
    for (var i in array) {
        if (array[i] == item) {
            array.splice(i, 1);
            break;
        }
    }
}
function Alldelete() {

    if ($('#tblCommunicationSent').find('input[type=checkbox]:checked').length == 0) {
        //alert('Please select atleast one checkbox');
        $('#MyModalcheckbox').modal('show');
        return false;
    }
    $('#MyModalDelete').modal('show');
}


function Deletemsg() {
    debugger;
    var messageids = '';
    var table = $('#tblCommunicationSent');
    /* table.DataTable().destroy();*/
    var isAllChecked = false;

    if ($("#example-select-all").is(":checked")) {
        isAllChecked = true;
    }
    var searchFilterValue = $(".form-control-sm").val();

    $(".loader-wrapper").addClass("show1");
    var UrlDeletemsg = $('#hdDeletemsg').val();
    $(".loader-wrapper").addClass("show1");
    var date = $("#DateParam").val();
    var subject = $("#SubjectParam").val();
    $.ajax({
        type: "Post",
        url: UrlDeletemsg,
        data: { /*messageids: messageids,*/ isAllChecked: isAllChecked, searchFilter: searchFilterValue, ids: AllCheckedRowsIds, date: date, subject: subject },
        success: function (result) {

            $('#MyModalDelete').modal('hide');
            $("div.deletemsgfailure").show();
            $("div.deletemsgfailure").fadeIn(300).delay(1500).fadeOut(4000);
            setTimeout(function () {

                var url = $(location).attr("href");
                window.location.href = url;
            }, 1000);


        },
        error: function (response) {

        }
    });
}


$("#chkAll").click(function () {

    //Determine the reference CheckBox in Header row.
    var chkAll = this;

    //Fetch all row CheckBoxes in the Table.
    var chkRows = $("#tblCommunicationSent").find(".chkRow");

    //Execute loop over the CheckBoxes and check and uncheck based on
    //checked status of Header row CheckBox.
    chkRows.each(function () {
        $(this)[0].checked = chkAll.checked;
    });
});

$(".chkRow").click(function () {

    //Determine the reference CheckBox in Header row.
    var chkAll = $("#chkAll");

    //By default set to Checked.
    chkAll.prop("checked", true);

    //Fetch all row CheckBoxes in the Table.
    var chkRows = $("#tblCommunicationSent").find(".chkRow");

    //Execute loop over the CheckBoxes and if even one CheckBox 
    //is unchecked then Uncheck the Header CheckBox.
    chkRows.each(function () {
        if (!$(this).is(":checked")) {
            //chkAll.removeAttr("checked", "checked");
            chkAll.prop("checked", false);
            return;
        }
    });
});

///New Conversation modal poup page js start

function insertmsgold() {

    //var customermail = $('#customermail').val();
    var BackofficeCustomerID = $('#txtCustomer').val();/*---Admin*/
    var ReasonTypeID = $('#ReasonTypeID').val();

    if (BackofficeCustomerID == null || BackofficeCustomerID == "" || BackofficeCustomerID == undefined) {
        var BackofficeCustomerID = $('#txtCustomerid option:selected').val();/*---Customer*/
    }

    var subject = $('#subject').val();
    var message = $('.Editor-editor').html();
    if (subject == "") {
        $('#subject').css('border-color', 'red');
        return false;
    }


    var formData = new FormData();
    formData.append("BackofficeCustomerID", BackofficeCustomerID);
    formData.append("Subject", subject);
    formData.append("Messages", message);
    formData.append("ReasonTypeID", ReasonTypeID);
    if ($(".fileclass")[0].files[0] != undefined) {
        formData.append($(".fileclass")[0].files[0].name, $(".fileclass")[0].files[0]);
    }

    var UrlNewConversation = $('#hdNewConversation').val();
    $.ajax({
        url: UrlNewConversation,
        contentType: false,
        processData: false,
        type: 'POST',
        data: formData,
        success: function (result) {

            $('#new-conversation').modal('hide');
            $("div.success").show();
            $("div.success").fadeIn(300).delay(1500).fadeOut(4000);
            setTimeout(function () {
                var url = $(location).attr("href");
                window.location.href = url;
            }, 1000);
        }
    });

}
rowCollection = [];
function allmsg(id) {

    var rowIndex = this.event.currentTarget.parentElement.parentElement.rowIndex;
    var rowIndex1 = this.event.currentTarget.parentElement.parentElement.rowIndex;
    if (rowCollection.indexOf(rowIndex) > -1) {
        $("#hdcustomerid").val(id);
        return false;
    }
    rowCollection.push(rowIndex);
    $("#hdcustomerid").val(id);
    $('#replydiv').css('display', 'none');
    $('#menuBarDiv_replymsg').css('display', 'none');
    $('table[id*="tblCommunication"] > tbody > tr').eq(--rowIndex1).removeClass("pink-bg");

    var UrlShowamessageDetails = $('#hdShowamessageDetails').val();
    $.ajax({
        type: "Post",
        url: UrlShowamessageDetails,
        data: { id: id },
        success: function (result) {

            $('#subjectdata').html('');
            //$('#subjectdata1').html('');
            if (result == 0) {
                $('#openre').css("visibility", "hidden");
                $('#reload').css("visibility", "hidden");
                //$('#inboxhedaer').css("visibility", "hidden");
            }
            else {

                var row = `<tr class="c" id="rowAppend_` + id + `"> <td colspan="8" class="inner-wrap">`;
                var row1 = `<tr class="c" id="rowAppend_` + id + `"> <td colspan="8" class="inner-wrap">`;
                var name = '';

                $.each(result, function (key, value) {


                    if (value.messageCount != 0) {
                        $('#MessageCountID').show();

                    }
                    else {
                        $('#MessageCountID').hide();


                    }
                    if (value.cusFirstName != null) {
                        name = value.cusFirstName + ' ' + value.cusLastName;
                    }
                    else {
                        name = value.adminFirstName + ' ' + value.adminLastName;
                    }

                    var downloaddiv = '<div class="col-md-6" style="border-bottom: 1px solid #bfbfbf; padding-bottom: 10px;"> &nbsp;<span><a href="/Communication/DownloadFile/?filename=' + value.fileupload + '" target="_blank"><i class="fa fa-download" style="color: #337ab7" aria-hidden="true"></i></a></span></div >';
                    var download;
                    if (value.fileupload != "") {
                        download = downloaddiv;
                    }
                    else {
                        download = '<div style="border-bottom: 1px solid #bfbfbf; padding-bottom: 10px;"> </div>';
                    }
                    row += `  ${key == 0 ? ` <div class="row mt-3">
                                    <div class="col-md-6 pt-left">
                                         &nbsp;
                                   </div>
                                       
                                   
<div class="col-md-6"> &nbsp;
                                    <a id="openre" onclick="OpenReplyBox()" style="cursor:pointer; display:inline-block; position:relative">
<img id="replyicon"  style="cursor:pointer;" src="/img_new/LReplyIcon.png" />
                                   </div> 
</div>

`: ``} 
<div class="row">
                                <div class="cst-subject col-md-12" style="padding-left:30px;line-height:20px;font-weight: 700;margin-top: 10px;">
                                         Sent <span  id="subjectdispaly">` + value.senttodate + `</span><br>
From : <span> `+ name + `</span>  
                                         
                                    </div>
                                    <div class="col-md-6 text-right f12">
                                        <span class="text-primary bold" id="fulldate" style="display:none">`+ value.createdDate1 + `</span>
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-lg-12" style="margin-left: 14px;">
                                        <p class="text-justify" id="fullmsg" style="padding: inherit; white-space: break-spaces;">`+ value.messages + `</p>
                                    </div>
  </div>


`+ download + `

`
                });

                row += `</td></tr>`;

                $('table[id*="tblCommunication"] > tbody > tr').eq(--rowIndex).after(row);

                //this for open reply pop
                $.each(result, function (key, value) {

                    if (value.cusFirstName != null) {
                        name = value.cusFirstName + ' ' + value.cusLastName;
                    }
                    else {
                        name = value.adminFirstName + ' ' + value.adminLastName;
                    }
                    row1 += `  ${key == 0 ? ` <div class="row mt-3">
                                    <div class="col-md-6 pt-left">
                                         &nbsp;
                                   </div>
                                       
                                   
<div class="col-md-6"> &nbsp;

                                   </div> 
</div>

`: ``} 
<div class="row">
                                <div class="cst-subject col-md-6" style="padding-left:30px;line-height:20px;font-weight: 700;margin-top: 10px;">
                                         Sent <span  id="subjectdispaly">` + value.senttodate + `</span><br>
From : <span> `+ name + `</span>  
                                         
                                    </div>
                                    <div class="col-md-6 text-right f12">
                                        <span class="text-primary bold" id="fulldate" style="display:none">`+ value.createdDate1 + `</span>
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-lg-12">
                                        <p class="text-justify bold" id="fullmsg" style="padding: inherit; white-space: break-spaces;">`+ value.messages + `</p>
                                    </div>
                                </div>
`
                });

                row1 += `</td></tr>`;
                $('#subjectdata').append(row1);

                $('#openre').css("visibility", "visible");
                $('#reload').css("visibility", "visible");
                $('#inboxhedaer').css("visibility", "visible");




            }
        }
    })

}
///New Conversation modal poup page js end
function allSentmsg(id) {
    var rowIndex = this.event.currentTarget.parentElement.parentElement.rowIndex;
    var rowIndex1 = this.event.currentTarget.parentElement.parentElement.rowIndex;
    if (rowCollection.indexOf(rowIndex) > -1) {
        $("#hdcustomerid").val(id);
        return false;
    }
    rowCollection.push(rowIndex);
    $("#hdcustomerid").val(id);
    $('#replydiv').css('display', 'none');
    $('#menuBarDiv_replymsg').css('display', 'none');
    $('table[id*="tblCommunication"] > tbody > tr').eq(--rowIndex1).removeClass("pink-bg");

    var UrlShowamessageDetails = $('#hdShowSentItemMessages').val();
    $.ajax({
        type: "Post",
        url: UrlShowamessageDetails,
        data: { id: id },
        success: function (result) {

            $('#subjectdata').html('');
            //$('#subjectdata1').html('');
            if (result == 0) {
                $('#openre').css("visibility", "hidden");
                $('#reload').css("visibility", "hidden");
                //$('#inboxhedaer').css("visibility", "hidden");
            }
            else {

                var row = `<tr class="c" id="rowAppend_` + id + `"> <td colspan="8" class="inner-wrap">`;
                var row1 = `<tr class="c" id="rowAppend_` + id + `"> <td colspan="8" class="inner-wrap">`;
                var name = '';

                $.each(result, function (key, value) {

                    if (value.cusFirstName != null) {
                        name = value.cusFirstName + ' ' + value.cusLastName;
                    }
                    else {
                        name = value.adminFirstName + ' ' + value.adminLastName;
                    }

                    var downloaddiv = '<div class="col-md-6" style="border-bottom: 1px solid #bfbfbf; padding-bottom: 10px;"> &nbsp;<span><a href="/Communication/DownloadFile/?filename=' + value.fileupload + '" target="_blank"><i class="fa fa-download" style="color: #337ab7" aria-hidden="true"></i></a></span></div >';
                    var download;
                    if (value.fileupload != "") {
                        download = downloaddiv;
                    }
                    else {
                        download = '<div style="border-bottom: 1px solid #bfbfbf; padding-bottom: 10px;"> </div>';
                    }


                    row += `  ${key == 0 ? ` <div class="row mt-3">
                                    <div class="col-md-6 pt-left">
                                         &nbsp;
                                   </div>
                                       
                                   
<div class="col-md-6"> &nbsp;
                                    <a id="openre" onclick="OpenReplyBox()" style="cursor:pointer; display:inline-block; position:relative">
<img id="replyicon"  style="cursor:pointer;" src="/img_new/LReplyIcon.png" />
</a>
                                   </div> 
</div>

`: ``} 
<div class="row">
                                <div class="cst-subject col-md-6" style="padding-left:30px;line-height:20px;font-weight: 700;margin-top: 10px;">
                                         Sent <span  id="subjectdispaly">` + value.senttodate + `</span><br>
From : <span> `+ name + `</span>  
                                         
                                    </div>
                                    <div class="col-md-6 text-right f12">
                                        <span class="text-primary bold" id="fulldate" style="display:none">`+ value.createdDate1 + `</span>
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-lg-12" style="margin-left: 14px;">
                                        <p class="text-justify" id="fullmsg" style="padding: inherit; white-space: break-spaces;">`+ value.messages + `</p>
                                    </div>
                                </div>
`+ download + `
`
                });

                row += `</td></tr>`;

                $('table[id*="tblCommunication"] > tbody > tr').eq(--rowIndex).after(row);

                //this for open reply pop
                $.each(result, function (key, value) {

                    if (value.cusFirstName != null) {
                        name = value.cusFirstName + ' ' + value.cusLastName;
                    }
                    else {
                        name = value.adminFirstName + ' ' + value.adminLastName;
                    }
                    row1 += `  ${key == 0 ? ` <div class="row mt-3">
                                    <div class="col-md-6 pt-left">
                                         &nbsp;
                                   </div>
                                       
                                   
<div class="col-md-6"> &nbsp;

                                   </div> 
</div>

`: ``} 
<div class="row">
                                <div class="cst-subject col-md-6" style="padding-left:30px;line-height:20px;">
                                         Sent <span  id="subjectdispaly">` + value.senttodate + `</span><br>
From : <span> `+ name + `</span>  
                                         
                                    </div>
                                    <div class="col-md-6 text-right f12">
                                        <span class="text-primary bold" id="fulldate" style="display:none">`+ value.createdDate1 + `</span>
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-lg-12" style="margin-left: 14px;">
                                        <p class="text-justify bold" id="fullmsg" style="padding: inherit; white-space: break-spaces;">`+ value.messages + `</p>
                                    </div>
                                </div>
`
                });

                row1 += `</td></tr>`;
                $('#subjectdata').append(row1);

                $('#openre').css("visibility", "visible");
                $('#reload').css("visibility", "visible");
                $('#inboxhedaer').css("visibility", "visible");




            }
        }
    })

}

function OpenReplyBox() {
    var replyidid = $("#hdcustomerid").val();
    var subject = $('#subjectdispaly').text();
    var urlhdReplyBoxdata = $('#hdReplyBoxdata').val();
    if (subject == "") {
        $("div.Subjectfailure").show();
        $("div.Subjectfailure").fadeIn(300).delay(1500).fadeOut(2000);
        return false;
    }
    else {
        $.ajax({
            type: "Post",
            url: urlhdReplyBoxdata,
            data: { replyidid: replyidid },
            success: function (result) {
                $('#replysubject').val('Re:' + result.subject);
                //$('#replycustomermail').val(result.customerEmail);
                $('#replydiv').modal('show')


            }
        })
        setMediaSelector('mediaSelector-Inbox');
    }
}

function Restpage() {
    setTimeout(function () {
        var url = $(location).attr("href");
        window.location.href = url;
    }, 300);
}



// reply from customer
function replymessagecustomer() {
    var hdcustomerid = $('#hdcustomerid').val();
    var replycustomermail = $('#replycustomermail').val();
    var replysubject = $('#replysubject').val();
    //var replymsg = $('#replymsg').val();
    var replymsg = $('.Editor-editor').text();
    var formData = new FormData();
    formData.append("CustomerEmail", replycustomermail);
    formData.append("Subject", replysubject);
    formData.append("Messages", replymsg);
    formData.append("MessageId", hdcustomerid);


    var UrlhdReplyConversation = $('#hdReplyConversation').val();
    $.ajax({
        url: UrlhdReplyConversation,
        contentType: false,
        processData: false,
        type: 'POST',
        data: formData,
        success: function (result) {

            $('#replydiv').modal('hide')
            $("div.success").show();
            $("div.success").fadeIn(300).delay(1500).fadeOut(1000);
            setTimeout(function () {
                var url = $(location).attr("href");
                window.location.href = url;
            }, 1000);
        }
    });
}
///AutoComplete Functionality
$(document).ready(function () {
    SearchText();
});
function SearchText() {



    // AutoComplete advance search page
    $("#txtCustomerCreate").autocomplete({
        source: function (request, response) {
            var UrlAutoComplete = $('#hdAutoComplete').val();
            $.ajax({

                type: "Post",
                url: UrlAutoComplete,
                data: { mykey: extractLast(request.term) },
                ajaxasync: true,

                dataType: "json",

                //
                ajaxasync: false,
                success: function (data) {
                    response(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            var terms = split(this.value);
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(", ");
            return false;
        }
    });
    $("#txtCustomerCreate").bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    })
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }

    /// AutoComplete Document Name From Libary

    $("#txtAutofromlibary").autocomplete({
        source: function (request, response) {
            var hdAutoCompleteDocument = $('#hdAutoCompleteDocument').val();
            $.ajax({

                type: "Post",
                url: hdAutoCompleteDocument,
                data: { mykey: extractLast(request.term) },
                ajaxasync: true,

                dataType: "json",

                //
                ajaxasync: false,
                success: function (data) {
                    response(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },

    });
    $("#txtAutofromlibary").bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    })
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }

    $("#txtAutofromlibaryr").autocomplete({
        source: function (request, response) {
            var hdAutoCompleteDocument = $('#hdAutoCompleteDocument').val();
            $.ajax({

                type: "Post",
                url: hdAutoCompleteDocument,
                data: { mykey: extractLast(request.term) },
                ajaxasync: true,

                dataType: "json",

                //
                ajaxasync: false,
                success: function (data) {
                    response(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },

    });
    $("#txtAutofromlibaryr").bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    })
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }
}

function UpdateExpireAdmin() {
    var unselectedid = '';
    var MessageIds = '';
    var selectedCloseId = '';
    var unselectedCloseId = '';
    MessageIds = $("#hdNeverExpirecheckedList").val();
    unselectedid = $("#hdNeverExpireUncheckList").val();
    selectedCloseId = $("#hdMsgClosecheckedList").val();
    //var table = $('#tblCommunication');
    //table.DataTable().destroy();
    //$("#tblCommunication TBODY TR").each(function (index, value) {
    //    var row = $(this);

    //    var checkstatus = row.closest('tr').find('.chkRowAdmin').prop('checked');
    //    if (checkstatus == true) {
    //        MessageIds += (!MessageIds ? row.find(".chkRowAdmin").val() : (',' + row.find(".chkRowAdmin").val()));

    //    }
    //    else {

    //        unselectedid += (!unselectedid ? row.find(".chkRowAdmin").val() : (',' + row.find(".chkRowAdmin").val()));
    //    }

    //    var checkstatusClose = row.closest('tr').find('.chkRowAdminClose').prop('checked');
    //    if (checkstatusClose == true) {
    //        selectedCloseId += (!selectedCloseId ? row.find(".chkRowAdminClose").val() : (',' + row.find(".chkRowAdminClose").val()));

    //    }
    //    else {

    //        unselectedCloseId += (!unselectedCloseId ? row.find(".chkRowAdminClose").val() : (',' + row.find(".chkRowAdminClose").val()));
    //    }

    //});
    MessageIds = $("#hdNeverExpirecheckedList").val();
    unselectedid = $("#hdNeverExpireUncheckList").val();
    $(".loader-wrapper").addClass("show1");
    var hdUpdateAdmin = $('#hdUpdateAdmin').val();
    $.ajax({
        type: "Post",
        url: hdUpdateAdmin,
        data: { MessageIds: MessageIds, unselectedid: unselectedid, selectedCloseId: selectedCloseId, unselectedCloseId: unselectedCloseId },
        success: function (result) {
            $("div.updatemsg").show();
            $(".loader-wrapper").removeClass("show1");
            $("#hdNeverExpirecheckedList").val('');
            $("#hdNeverExpireUncheckList").val('');
            setTimeout(function () {

                var url = $(location).attr("href");
                window.location.href = url;
            }, 1000);


        },
        error: function (response) {

        }
    });
}
function UpdateCustomerExpire() {
    var unselectedid = '';
    var MessageIds = '';
    var selectedCloseId = '';
    var unselectedCloseId = '';
    MessageIds = $("#hdNeverExpirecheckedList").val();
    unselectedid = $("#hdNeverExpireUncheckList").val();
    selectedCloseId = $("#hdMsgClosecheckedList").val();
    //var table = $('#tblCommunication');
    //table.DataTable().destroy();
    //$("#tblCommunication TBODY TR").each(function (index, value) {
    //    var row = $(this);

    //    var checkstatus = row.closest('tr').find('.chkRowCustomer').prop('checked');
    //    if (checkstatus == true) {
    //        MessageIds += (!MessageIds ? row.find(".chkRowCustomer").val() : (',' + row.find(".chkRowCustomer").val()));


    //    }
    //    else {

    //        unselectedid += (!unselectedid ? row.find(".chkRowCustomer").val() : (',' + row.find(".chkRowCustomer").val()));
    //    }
    //    var checkstatusClose = row.closest('tr').find('.chkRowAdminClose').prop('checked');
    //    if (checkstatusClose == true) {
    //        selectedCloseId += (!selectedCloseId ? row.find(".chkRowAdminClose").val() : (',' + row.find(".chkRowAdminClose").val()));

    //    }
    //    else {

    //        unselectedCloseId += (!unselectedCloseId ? row.find(".chkRowAdminClose").val() : (',' + row.find(".chkRowAdminClose").val()));
    //    }

    //});
    $(".loader-wrapper").addClass("show1");
    var hdUpdateCustomer = $('#hdUpdateCustomer').val();
    $.ajax({
        type: "Post",
        url: hdUpdateCustomer,
        data: { MessageIds: MessageIds, unselectedid: unselectedid, selectedCloseId: selectedCloseId, unselectedCloseId: unselectedCloseId },
        success: function (result) {
            $("div.updatemsg").show();
            $(".loader-wrapper").removeClass("show1");
            $("#hdNeverExpirecheckedList").val('');
            $("#hdNeverExpireUncheckList").val('');
            setTimeout(function () {

                var url = $(location).attr("href");
                window.location.href = url;
            }, 1000);


        },
        error: function (response) {

        }
    });
}
function AddForNeverExpirechecked(checkbox, MessageId) {
    if ($(checkbox).is(':checked')) {
        var hdcheckedList = $("#hdNeverExpirecheckedList").val();

        if (hdcheckedList != "") {
            hdcheckedList = hdcheckedList + ',' + MessageId;
        } else
            hdcheckedList = MessageId;

        $("#hdNeverExpirecheckedList").val(hdcheckedList);
    }
    else {
        var single = $("#hdNeverExpirecheckedList").val()
        var delList = $("#hdNeverExpirecheckedList").val().split(',');
        var index = delList.indexOf(MessageId.toString());
        if (index > -1) {
            delList.splice(index, 1);
        }
        var bbb = delList.toString().replace(/,/g, ',')
        $("#hdNeverExpirecheckedList").val(bbb);
    }
}
function AddForNeverExpireuncheck(checkbox, MessageId) {

    if ($(checkbox).is(':checked')) {
        var single = $("#hdNeverExpireUncheckList").val()
        var delList = $("#hdNeverExpireUncheckList").val().split(',');
        var index = delList.indexOf(MessageId.toString());
        if (index > -1) {
            delList.splice(index, 1);
        }
        var bbb = delList.toString().replace(/,/g, ',')
        $("#hdNeverExpireUncheckList").val(bbb);
    }
    else {

        var hdUncheckList = $("#hdNeverExpireUncheckList").val();

        if (hdUncheckList != "") {
            hdUncheckList = hdUncheckList + ',' + MessageId;
        } else
            hdUncheckList = MessageId;

        $("#hdNeverExpireUncheckList").val(hdUncheckList);
    }
}
function AddForMsgClosechecked(checkbox, MessageId) {
    if ($(checkbox).is(':checked')) {
        var hdcheckedList = $("#hdMsgClosecheckedList").val();

        if (hdcheckedList != "") {
            hdcheckedList = hdcheckedList + ',' + MessageId;
        } else
            hdcheckedList = MessageId;

        $("#hdMsgClosecheckedList").val(hdcheckedList);
    }
    else {
        var single = $("#hdMsgClosecheckedList").val()
        var delList = $("#hdMsgClosecheckedList").val().split(',');
        var index = delList.indexOf(MessageId.toString());
        if (index > -1) {
            delList.splice(index, 1);
        }
        var bbb = delList.toString().replace(/,/g, ',')
        $("#hdMsgClosecheckedList").val(bbb);
    }
}
//Advance Search
function AdvanceSearchPOP() {

    $("#MyModalAdvanceSearch").show();
    return false;
}
function radioName(Name) {
    $("#lblAdvanceSearch").text(Name);
    var radioValue = $("input[name='radioName']:checked").val();
    if (Name == 'Skip') {
        $("#txtAdvanceSearch").hide();
        $("#txtAdvanceSearch").val('');
        $("#ddlskip").show();
    }
    else {
        $("#ddlskip").hide();
        $("#txtAdvanceSearch").show();
    }

}
function AdvanceSearchPOPClose() {
    $("#MyModalAdvanceSearch").hide();
}
function AdvanceSearchValue() {
    var Value = "";


    var ColumnName = $("input[name='radioName']:checked").val();
    if (ColumnName == undefined) {
        $('#txtAdvanceSearch').css('border-color', 'red');
        $('#txtAdvanceSearch').focus();
        return false;
    }
    if (ColumnName == 'SkipService') {
        Value = $('#ddlskip').val();
    }
    else {
        Value = $('#txtAdvanceSearch').val();
    }

    $('#txtAdvanceSearch').css('border-color', 'none');


    var hdAdvanceSearch = $('#hdAdvanceSearch').val();
    $.ajax({
        type: "Post",
        url: hdAdvanceSearch,
        data: { ColumnName: ColumnName, Value: Value },
        dataType: "json",
        success: function (response) {

            $('#tblAdvanceSearch').dataTable({
                data: response,
                "columns": [


                    {
                        "render": function (data, type, full, meta) {

                            return '<input class="chkRowadvance" value="' + full.backOfficeCustomerID + '"  onclick="return AddForcheckAdvanceSearch(this,\'' + full.backOfficeCustomerID.toString() + '\')" class="chkBox" type="checkbox" />';
                        }
                    },
                    { "data": "backOfficeCustomerID" },
                    { "data": "name" },
                    { "data": "emailId" },
                    { "data": "serviceAccountNumber" },
                    { "data": "address" }



                ],
                language: {
                    "search": '<i class="fa fa-search"></i>',
                    searchPlaceholder: "Search records",
                    paginate: {
                        next: '<i class="fa fa-angle-right"></i>',
                        previous: '<i class="fa fa-angle-left"></i>'
                    }
                },
                "lengthChange": false,
                "paging": true,
                "info": true,
                "iDisplayLength": 10,
                "bDestroy": true


            });
            //$('#tblAdvanceSearch').DataTable().destroy();

            setTimeout(function () {

            }, 1000);
        },
        error: function (response) {

        }
    });
}
function AddForcheckAdvanceSearch(checkbox, BackOfficeCustomerID) {
    if ($(checkbox).is(':checked')) {
        var hdcheckedList = $("#txtCustomer").val();

        if (hdcheckedList != "") {
            hdcheckedList = hdcheckedList + ',' + BackOfficeCustomerID;
        } else
            hdcheckedList = BackOfficeCustomerID;

        $("#txtCustomer").val(hdcheckedList);
    }
    else {
        var single = $("#txtCustomer").val()
        var delList = $("#txtCustomer").val().split(',');
        var index = delList.indexOf(BackOfficeCustomerID.toString());
        if (index > -1) {
            delList.splice(index, 1);
        }
        var bbb = delList.toString().replace(/,/g, ',')
        $("#txtCustomer").val(bbb);
    }
}

$("#chkAll").click(function () {

    //Determine the reference CheckBox in Header row.
    var chkAll = this;

    //Fetch all row CheckBoxes in the Table.
    var chkRows = $("#tblAdvanceSearch").find(".chkRowadvance");

    //Execute loop over the CheckBoxes and check and uncheck based on
    //checked status of Header row CheckBox.
    chkRows.each(function () {
        $(this)[0].checked = chkAll.checked;

    });
    if (chkAll.checked == true) {
        var values = "";

        $("#tblAdvanceSearch TBODY TR").each(function (index, value) {
            var row = $(this);

            var checkstatus = row.closest('tr').find('.chkRowadvance').prop('checked');

            values += (!values ? row.find(".chkRowadvance").val() : (',' + row.find(".chkRowadvance").val()));
            $("#txtCustomer").val(values);
        });

    }
    else {
        $("#txtCustomer").val(values);
    }
});


function show(input) {

    var validExtensions = ['jpg', 'png', 'PNG', 'jpeg', '.gif', 'doc', 'docx', 'pdf', 'xls', 'xlsx']; //array of valid extensions
    var fileName = input.files[0].name;
    var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
    if ($.inArray(fileNameExt, validExtensions) == -1) {
        input.type = ''
        input.type = 'file'

        alert("Only these file types are accepted : " + validExtensions.join(', '));
    }

}

//Check Box Check Box hide show Upload
//function CheckBoxhideshowUploadr(Name) {


//    if (Name == 'fromdrive') {

//        $("#fromdriver").show();
//        $("#btnfromdriver").show();
//        $("#fromlibaryr").hide();
//        $("#btnfromlibaryr").hide();
//    }
//    else {
//        $("#fromdriver").hide();
//        $("#btnfromdriver").hide();
//        $("#fromlibaryr").show();
//        $("#btnfromlibaryr").show();
//    }

//}

///validation file type for customer
function Checkfiletype(input) {

    var file_size = input.files[0].size;
    if (file_size > 10097152) {
        input.type = ''
        input.type = 'file'

        input.type = ''
        input.type = 'file'

        swal({

            text: "File size is greater than 10MB.",
            icon: "error",

        })
            .then((ok) => {
                if (ok) {


                }
            });

        return false;
    }

    var validExtensions = ['jpg', 'png', 'PNG', 'jpeg', '.gif', 'pdf', 'doc', 'docx', 'xls', 'xlsx']; //array of valid extensions
    var fileName = input.files[0].name;
    var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
    if ($.inArray(fileNameExt, validExtensions) == -1) {
        input.type = ''
        input.type = 'file'

        swal({

            text: "The filetype is not recognized for use in this system.  Please change you filetype or contact us for additional file transfer options.",
            icon: "error",

        })
            .then((ok) => {
                if (ok) {


                }
            });

    }


}

