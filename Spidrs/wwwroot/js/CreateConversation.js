﻿var customersAll;
var customers = [];
var allFilters = [];
var Autofromlibary = ''
$(document).ready(function () {
    $("#ddlskipCreate").hide();
    $("#fromlibary").hide();
    $("#btnfromlibary").hide();
    var hdGetReasonType = $('#hdGetReasonType').val();
    $.ajax({
        type: "GET",
        url: hdGetReasonType,
        data: "{}",
        success: function (data) {
            var s = '<option value="-1">Select Reason Type</option>';
            for (var i = 0; i < data.length; i++) {
                s += '<option value="' + data[i].value + '">' + data[i].text + '</option>';
            }
            $("#ddlReasonTypeID").html(s);
        }
    });

    var hdGetServiceType = $('#hdGetServiceType').val();
    $.ajax({
        type: "GET",
        url: hdGetServiceType,
        data: "{}",
        success: function (data) {
            var s = '<option value="-1">Select Service Type</option>';
            for (var i = 0; i < data.length; i++) {
                s += '<option value="' + data[i].value + '">' + data[i].text + '</option>';
            }
            $("#ddlServiceTypeID").html(s);
        }
    });

    $("#chkAllCreate").click(function () {

        //Determine the reference CheckBox in Header row.
        var chkAll = this;

        //Fetch all row CheckBoxes in the Table.
        var chkRows = $("#tblAdvanceSearchCreate").find(".chkRowadvance");

        //Execute loop over the CheckBoxes and check and uncheck based on
        //checked status of Header row CheckBox.
        chkRows.each(function () {
            $(this)[0].checked = chkAll.checked;

        });
        if (chkAll.checked == true) {
            var values = "";

            $("#tblAdvanceSearchCreate TBODY TR").each(function (index, value) {
                var row = $(this);

                var checkstatus = row.closest('tr').find('.chkRowadvance').prop('checked');

                values += (!values ? row.find(".chkRowadvance").val() : (',' + row.find(".chkRowadvance").val()));
                $("#txtCustomerCreate").val(values);
            });

        }
        else {
            $("#txtCustomerCreate").val(values);
        }
    });

    var hdGetFilterFields = $('#hdGetFilterFields').val();
    $.ajax({
        type: "GET",
        url: hdGetFilterFields,
        data: "{}",
        success: function (data) {
            var s = '<option value="-1">Select</option>';
            for (var i = 0; i < data.length; i++) {
                s += '<option value="' + data[i].text + '">' + data[i].text + '</option>';
            }
            $("[name=ddlFieldList]").html(s);
        }
    });

    $("#notificationModel").on("hidden.bs.modal", function () {
        $("#MyModalCreateconversation").show();
    });

    $("#filterTable select, #filterTable .filterText").change(function () {
        isChanged = true;
        $("#btnfromdrive").prop('disabled', true);
    });

    $("#titleModel").on("hidden.bs.modal", function () {
        $("#MyModalCreateconversation").modal('show');
    });

    //$("[name=ddlFieldList]").change(function () {
    //    if (parseInt($(this).val()) > -1) {
    //        $("[name=ddlFieldList]").not(this).find("[value=" + parseInt($(this).val()) + "]").remove();
    //    }
    //    else if (parseInt($(this).val()) == -1) {

    //    }
    //});


});

//$(document).on('click','.buttons-pdf', function () {
//    $("#rowCount").html($("#customerListTable tbody tr.selected").length);
//});

function AdvanceSearchPOPCreateConversation() {

    $("#MyModalAdvanceSearchOnCreateConversation").show();
    return false;
}
function radioNameCreate(Name) {
    $("#lblAdvanceSearchCreate").text(Name);
    var radioValue = $("input[name='radioNameCreate']:checked").val();
    if (Name == 'Skip') {
        $("#txtAdvanceSearchCreate").hide();
        $("#txtAdvanceSearchCreate").val('');
        $("#ddlskipCreate").show();
    }
    else {
        $("#ddlskipCreate").hide();
        $("#txtAdvanceSearchCreate").show();
    }

}
//function AdvanceSearchPOPCloseCreate() {
//    $("#MyModalAdvanceSearchOnCreateConversation").hide();
//    $("#MyModalCreateconversation").show();
//    $('#menuBarDiv_Newmessage').css('display', 'none');
//}

function CreatenewCreateConversationPOPUP() {
    $("#MyModalCreateconversation").show();
    $('#menuBarDiv_Newmessage').css('display', 'none');

}

function CloseMessagePOPUP() {
    $("#MyModalMessage").hide();
}

function AdvanceSearchValueCreate() {
    var Value = "";
    var ColumnName = $("input[name='radioNameCreate']:checked").val();
    if (ColumnName == undefined) {
        $('#txtAdvanceSearchCreate').css('border-color', 'red');
        $('#txtAdvanceSearchCreate').focus();
        return false;
    }
    if (ColumnName == 'SkipService') {
        Value = $('#ddlskipCreate').val();
    }
    else {
        Value = $('#txtAdvanceSearchCreate').val();
    }

    $('#txtAdvanceSearchCreate').css('border-color', 'none');


    var hdAdvanceSearch = $('#hdAdvanceSearchCreate').val();
    $.ajax({
        type: "Post",
        url: hdAdvanceSearch,
        data: { ColumnName: ColumnName, Value: Value },
        dataType: "json",
        success: function (response) {

            $('#tblAdvanceSearchCreate').dataTable({
                data: response,
                "columns": [


                    {
                        "render": function (data, type, full, meta) {

                            return '<input class="chkRowadvance" value="' + full.backOfficeCustomerID + '"  onclick="return AddForcheckAdvanceSearchCreate(this,\'' + full.backOfficeCustomerID.toString() + '\')" class="chkBox" type="checkbox" />';
                        }
                    },
                    { "data": "backOfficeCustomerID" },
                    { "data": "name" },
                    { "data": "emailId" },
                    { "data": "serviceAccountNumber" },
                    { "data": "address" }



                ],
                language: {
                    "search": '<i class="fa fa-search"></i>',
                    searchPlaceholder: "Search records",
                    paginate: {
                        next: '<i class="fa fa-angle-right"></i>',
                        previous: '<i class="fa fa-angle-left"></i>'
                    }
                },
                "lengthChange": true,
                "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                "paging": true,
                "info": true,
                "iDisplayLength": 10,
                "bDestroy": true


            });
            //$('#tblAdvanceSearch').DataTable().destroy();

            setTimeout(function () {

            }, 1000);
        },
        error: function (response) {

        }
    });
}
function AddForcheckAdvanceSearchCreate(checkbox, BackOfficeCustomerID) {
    if ($(checkbox).is(':checked')) {
        var hdcheckedList = $("#txtCustomerCreate").val();

        if (hdcheckedList != "") {
            hdcheckedList = hdcheckedList + ',' + BackOfficeCustomerID;
        } else
            hdcheckedList = BackOfficeCustomerID;

        $("#txtCustomerCreate").val(hdcheckedList);
    }
    else {
        var single = $("#txtCustomerCreate").val()
        var delList = $("#txtCustomerCreate").val().split(',');
        var index = delList.indexOf(BackOfficeCustomerID.toString());
        if (index > -1) {
            delList.splice(index, 1);
        }
        var bbb = delList.toString().replace(/,/g, ',')
        $("#txtCustomerCreate").val(bbb);
    }
}

;
///New Conversation modal poup page js start

function insertmsg() {
    debugger;
    //var customermail = $('#customermail').val();
    var BackofficeCustomerID = customers.join(','); //$('#txtCustomerCreate').val();/*---Admin*/
    //var ReasonTypeID = $('#ddlReasonTypeID').val();

    if (BackofficeCustomerID == null || BackofficeCustomerID == "" || BackofficeCustomerID == undefined) {
        var BackofficeCustomerID = $('#txtCustomerCreate option:selected').val();/*---Customer*/
        if (BackofficeCustomerID == null || BackofficeCustomerID == "" || BackofficeCustomerID == undefined) {
            BackofficeCustomerID = $('#ddlServiceTypeID option:selected').val();
        }
    }

    var subject = $('#subject').val();
    var message = $('#msg-wrapper .Editor-editor').html();
    if (subject == "") {
        $('#subject').css('border-color', 'red');
        return false;
    }

    debugger;
    var formData = new FormData();
    formData.append("BackofficeCustomerID", BackofficeCustomerID);
    formData.append("MessageTrackingCustomerId", BackofficeCustomerID);
    formData.append("Subject", subject);
    formData.append("Messages", message);
    //   formData.append("ReasonTypeID", ReasonTypeID);
    if ($(".fileclass")[0].files[0] != undefined) {
        formData.append($(".fileclass")[0].files[0].name, $(".fileclass")[0].files[0]);
    }

    var UrlNewConversation = $('#hdNewConversation').val();

    $(".loader-wrapper").addClass("show2");
    $.ajax({
        url: UrlNewConversation,
        contentType: false,
        processData: false,
        type: 'POST',
        data: formData,
        success: function (result) {

            $("#MyModalCreateconversation").modal('hide');
            $("#MyModalMessage").show();
            $(".loader-wrapper").removeClass("show2");
            //$("div.success").show();
            //$("div.success").fadeIn(300).delay(1500).fadeOut(4000);
            setTimeout(function () {
                var url = $(location).attr("href");
                $(".loader-wrapper").removeClass("show2");
                window.location.href = url;
            }, 1000);
        }
    });

}

getFileIcon = function (extension) {
    switch (extension) {
        case "doc":
        case "docx":
        case "odt":
            return "word";
        case "xls":
        case "xlsx":
        case "csv":
            return "excel";
        case "pdf":
            return "pdf";
        case "txt":
            return "text";
        case "zip":
        case "rar":
        case "7z":
            return "rar";
        case "ppt":
        case "pptx":
            return "powerpoint";
        case "mov":
        case "flv":
        case "mp4":
        case "wmv":
        case "avi":
        case "mpeg":
        case "webm":
        case "ogv":
            return "video";
        case "mp3":
            return "mp3";
        case "htm":
        case "html":
            return "html";
        default:
            return "fa fa-file";
    }
}

function selectLibrary(value, filePath) {
    var validExtensions = ['jpg', 'png', 'jpeg', 'gif', 'bmp'];
    var fileNameExt = filePath.substr(filePath.lastIndexOf('.') + 1);
    if ($.inArray(fileNameExt, validExtensions) > -1) {
        $('#fromlibary img').attr('src', filePath);
    }
    else {
        $('#fromlibary img').attr('src', '/img_new/files_icon/' + getFileIcon(fileNameExt) + '.png');
    }

    $('#fromlibary a.cross').show();
    Autofromlibary = value;
    $('#mediaModel').modal('hide');
}

function deleteLibrary() {
    $('#fromlibary img').attr('src', '/img_new/files_icon/fileUpload.png');
    $('#fromlibary a.cross').hide();
    Autofromlibary = '';
}

function insertmsgL() {
    //var customermail = $('#customermail').val();
    var BackofficeCustomerID = customers.join(','); //$('#txtCustomerCreate').val();/*---Admin*/

    if (BackofficeCustomerID == null || BackofficeCustomerID == "" || BackofficeCustomerID == undefined) {
        var BackofficeCustomerID = $('#txtCustomerCreate option:selected').val();/*---Customer*/
    }

    var subject = $('#subject').val();
    var message = $('#msg-wrapper .Editor-editor').html();
    if (subject == "") {
        $('#subject').css('border-color', 'red');
        return false;
    }


    var formData = new FormData();
    formData.append("BackofficeCustomerID", BackofficeCustomerID);
    formData.append("MessageTrackingCustomerId", BackofficeCustomerID);
    formData.append("Subject", subject);
    formData.append("Messages", message);
    formData.append("Autofromlibary", Autofromlibary);

    var UrlNewConversation = $('#hdNewConversationL').val();

    $(".loader-wrapper").addClass("show2");
    $.ajax({
        url: UrlNewConversation,
        contentType: false,
        processData: false,
        type: 'POST',
        data: formData,
        success: function (result) {
            $("#MyModalCreateconversation").modal('hide');
            $("#MyModalMessage").show();
            $(".loader-wrapper").removeClass("show2");
            setTimeout(function () {
                var url = $(location).attr("href");
                $(".loader-wrapper").removeClass("show2");
                window.location.href = url;
            }, 1000);
        }
    });

}

var isDrive = true;

function previewmsg() {
    if (isDrive) {
        $("#notificationModel .modal-title").html($("#subject").val());
        $("label[id='lblMessage']").html($('#msg-wrapper .Editor-editor').html());
        $('#notificationModel').modal('show');
    }
    else {
        var formData = new FormData();
        formData.append("Subject", $("#subject").val());
        formData.append("Messages", $('#msg-wrapper .Editor-editor').html());
        formData.append("Autofromlibary", Autofromlibary);
        $.ajax({
            url: '/customers/_notificationPreview',
            contentType: false,
            processData: false,
            type: 'POST',
            data: formData,
            success: function (result) {
                $('#notificationModelLibrary').html(result);
                $('#dashboardNotificationModel').modal('show');
            }
        });
    }

}


function replymessage() {
    var hdcustomerid = $('#hdcustomerid').val();
    var replycustomermail = $('#replycustomermail').val();
    var replysubject = $('#replysubject').val();
    //var replymsg = $('#replymsg').val();
    var replymsg = $('.Editor-editor').html();
    var formData = new FormData();
    formData.append("CustomerEmail", replycustomermail);
    formData.append("Subject", replysubject);
    formData.append("Messages", replymsg);
    formData.append("MessageId", hdcustomerid);

    if ($(".fileclass")[0].files[0] != undefined) {
        formData.append($(".fileclass")[0].files[0].name, $(".fileclass")[0].files[0]);
    }
    $(".loader-wrapper").addClass("show2");
    var UrlhdReplyConversation = $('#hdReplyConversation').val();
    $.ajax({
        url: UrlhdReplyConversation,
        contentType: false,
        processData: false,
        type: 'POST',
        data: formData,
        success: function (result) {
            $(".loader-wrapper").removeClass("show2");
            $('#replydiv').modal('hide')
            $("div.success").show();
            $("div.success").fadeIn(300).delay(1500).fadeOut(1000);

            setTimeout(function () {
                $(".loader-wrapper").removeClass("show2");
                var url = $(location).attr("href");
                window.location.href = url;
            }, 1000);
        }
    });
}

function replymessageL() {

    var hdcustomerid = $('#hdcustomerid').val();
    var replycustomermail = $('#replycustomermail').val();
    var replysubject = $('#replysubject').val();
    //var replymsg = $('#replymsg').val();
    var replymsg = $('.Editor-editor').html();
    var formData = new FormData();
    formData.append("CustomerEmail", replycustomermail);
    formData.append("Subject", replysubject);
    formData.append("Messages", replymsg);
    formData.append("MessageId", hdcustomerid);
    formData.append("Autofromlibary", Autofromlibary);
    $(".loader-wrapper").addClass("show2");
    var UrlhdReplyConversation = $('#hdReplyConversationL').val();
    $.ajax({
        url: UrlhdReplyConversation,
        contentType: false,
        processData: false,
        type: 'POST',
        data: formData,
        success: function (result) {

            $(".loader-wrapper").removeClass("show2");
            $('#replydiv').modal('hide')
            $("div.success").show();
            $("div.success").fadeIn(300).delay(1500).fadeOut(1000);
            setTimeout(function () {
                $(".loader-wrapper").removeClass("show2");
                var url = $(location).attr("href");
                window.location.href = url;
            }, 1000);
        }
    });
}

//Check Box Check Box hide show Upload
function CheckBoxhideshowUpload(Name) {

    if (Name == 'fromdrive') {
        isDrive = true;
        $("#fromdrive").show();
        $("#btnfromdrive").show();
        $("#fromlibary").hide();
        $("#btnfromlibary").hide();
    }
    else {
        isDrive = false;
        $("#fromdrive").hide();
        $("#btnfromdrive").hide();
        $("#fromlibary").show();
        $("#btnfromlibary").show();
    }

}

///validation file type for customer
function CheckfiletypeonCreateconversationforcustomer(input) {

    var file_size = input.files[0].size;
    if (file_size > 10097152) {
        input.type = ''
        input.type = 'file'

        input.type = ''
        input.type = 'file'

        swal({

            text: "File size is greater than 10MB.",
            icon: "error",

        })
            .then((ok) => {
                if (ok) {


                }
            });

        return false;
    }
    var validExtensions = ['jpg', 'png', 'PNG', 'jpeg', '.gif', 'pdf', 'doc', 'docx', 'xls', 'xlsx']; //array of valid extensions
    var fileName = input.files[0].name;
    var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
    if ($.inArray(fileNameExt, validExtensions) == -1) {
        input.type = ''
        input.type = 'file'

        swal({

            text: "The filetype is not recognized for use in this system.  Please change you filetype or contact us for additional file transfer options.",
            icon: "error",

        })
            .then((ok) => {
                if (ok) {


                }
            });

    }


}

// validation for file size

function Checkfiletsize(input) {

    var file_size = input.files[0].size;
    if (file_size > 10097152) {
        input.type = ''
        input.type = 'file'

        input.type = ''
        input.type = 'file'

        swal({

            text: "File size is greater than 10MB.",
            icon: "error",

        })
            .then((ok) => {
                if (ok) {


                }
            });

        return false;
    }
}

function getFilterdData() {
    var filters = getFilterArray();
    if (filters.length > -1) {
        $.ajax({
            url: "/Communication/FiltersForConversation",
            type: 'POST',
            data: { data: JSON.stringify(filters), customerType: $("[name='CustomerType']:checked").val() },
            success: function (result) {
                customers = result;
                customersAll = result;
                if (result.length > 0) {
                    $("#btnfromdrive").prop('disabled', false);
                }
                else {
                    $("#btnfromdrive").prop('disabled', true);
                }
                if (result.length == 1) {
                    $("#lblCustomerCount").html(result.length + " Customer found.");
                }
                else {
                    $("#lblCustomerCount").html(result.length + " Customers found.");
                }
                setSelectedCustomers(result.length);
            }
        });
    }
}

function setSelectedCustomers(length) {
    if (length == 1) {
        $("#lblSelectedCustomers").html(length + " Customer is selected.");
    }
    else {
        $("#lblSelectedCustomers").html(length + " Customers are selected.");
    }
}

function getFilterArray() {
    var filters = [];
    $("#filterTable tbody tr").each(function () {
        var fieldList = $(this).find(".filterFieldList").val();
        var operator = $(this).find(".filterOperator").val();
        var filterText = $(this).find(".filterText").val();

        if (fieldList != "-1" && fieldList != undefined && filterText != "" && filterText != undefined) {
            var values = {
                fieldName: fieldList,
                operator: operator,
                filterText: filterText,
            }
            filters.push(values);
        }
    });
    return filters;
}

function clearFilterdData() {
    $(".filterFieldList").val(-1);
    $("[name=ddlOperator]").val('=');
    $("[name=txtFilter]").val('');
    $("#lblCustomerCount").html('');
    $("#lblSelectedCustomers").html('');
    customers = [];
    $("#active").prop("checked", true);
}

function clearConversastionModel() {
    $("#btnfromdrive").prop('disabled', true);
    setMediaSelector('mediaSelector-Layout');
    clearFilterdData();
    $('#subject').val('');
    $('#msg-wrapper .Editor-editor').html('');
}

function clearCustomerConversastionModel() {
    setMediaSelector('mediaSelector-Layout');
    clearFilterdData();
    $('#subject').val('');
    $('#msg-wrapper .Editor-editor').html('');
}

function setMediaSelector(element) {
    $('#mediaSelector-Inbox, #mediaSelector-Layout').html('');
    $.ajax({
        url: '/Communication/mediaSelectorPartial',
        contentType: false,
        processData: false,
        type: 'GET',
        success: function (result) {
            $('#' + element).html(result);
            document.querySelector('#fileupload').value = "";
            $("#thumbnil, #notification-img").attr('src', '').hide();

            deleteLibrary();
            $('.' + element + ' input:radio[name="radioDocumentupload"][value="fromdrive"]').prop('checked', true);
            $("#fromdrive").show();
            $("#fromlibary").hide();
            Autofromlibary = '';

        },
        error: function (error) {
        }
    });
}


function saveMessage() {
    var subject = $('#subject').val();
    if (subject == "") {
        $('#subject').css('border-color', 'red');
        return false;
    }

    $("#MyModalCreateconversation").modal('hide');
    $('#titleModel').modal('show');
}

function submitMessage() {
    var title = $('#txtMessageTitle').val();
    var subject = $('#subject').val();
    var message = $('#msg-wrapper .Editor-editor').html();
    if (subject == "") {
        $('#subject').css('border-color', 'red');
        return false;
    }
    var filters = getFilterArray();
    var formData = new FormData();
    formData.append("Subject", subject);
    formData.append("Title", title);
    formData.append("Body", message);
    formData.append("DocumentType", $('[name="radioDocumentupload"]:checked').val());
    formData.append("Autofromlibary", Autofromlibary);
    formData.append("Filters", JSON.stringify(filters));

    if ($(".fileclass")[0].files[0] != undefined) {
        formData.append($(".fileclass")[0].files[0].name, $(".fileclass")[0].files[0]);
    }
    $.ajax({
        url: '/MessageHistory/Post',
        contentType: false,
        processData: false,
        type: 'POST',
        data: formData,
        success: function (result) {
            $('#txtMessageTitle').val('');
            $('#titleModel').modal('hide');
            $("div.success-message-history").show();
            $("div.success-message-history").fadeIn(300).delay(1500).fadeOut(4000);
        },
        error: function (error) {
            $("#overrideMessage-modal").modal('show');
        }
    });
}

function updateMessage() {
    var title = $('#txtMessageTitle').val();
    var subject = $('#subject').val();
    var message = $('#msg-wrapper .Editor-editor').html();

    var filters = getFilterArray();

    var formData = new FormData();
    formData.append("Subject", subject);
    formData.append("Title", title);
    formData.append("Body", message);
    formData.append("DocumentType", $('[name="radioDocumentupload"]:checked').val());
    formData.append("Autofromlibary", Autofromlibary);
    formData.append("Filters", JSON.stringify(filters));

    if ($(".fileclass")[0].files[0] != undefined) {
        formData.append($(".fileclass")[0].files[0].name, $(".fileclass")[0].files[0]);
    }
    $.ajax({
        url: '/MessageHistory/Update',
        contentType: false,
        processData: false,
        type: 'POST',
        data: formData,
        success: function (result) {
            if (result) {
                $('#titleModel').modal('hide');
                $("div.success-message-history").html('Message has been updated successfully.').show();
                $("div.success-message-history").fadeIn(300).delay(1500).fadeOut(4000);
            }

        },
        error: function (error) {
            $("div.success-message-history").html(error).show();
        }
    });
}
function formatDate(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
}

var table = $('#customerListTable').DataTable();

function getCustomerList() {

    var currentDate = new Date();
    var d = formatDate(currentDate);
    table.destroy();
    var filters = getFilterArray();
    var title = OrganizeTitle(filters);
    $('#customerListModel .modal-body').html('');
    $.ajax({
        url: '/Customers/_CustomerPartial',
        type: 'POST',
        data: { data: JSON.stringify(customersAll) },
        success: function (result) {

            $('#customerListModel .modal-body').html(result);

            $('#customerListTable').DataTable({
                "bPaginate": false,
                "lengthChange": false,
                "info": true,
                dom: 'Bfrtip',
                "buttons": [
                    {
                        extend: 'pdf',

                        customize: function (doc) {
                            doc['footer'] = (function (page, pages) {
                                return {

                                    columns: [
                                        {

                                            alignment: 'left',
                                            text: [

                                                { text: d.toString() },
                                                '                                                                                                                                                                           ',
                                                { text: page.toString() },
                                                ' of ',
                                                { text: pages.toString() }
                                            ]

                                        }],

                                    margin: [10, 0]
                                }
                            });
                            doc.content[1].margin = [20, 0, 20, 0] //left, top, right, bottom
                        },
                        footer: true,
                        exportOptions: {
                            columns: 'th:not(:first-child)'

                        },
                        title: 'QM Report\n\n' + title,
                        className: 'fa fa-download btn-green fld-tb',
                        text: '<i class=""></i> Download',
                        pageSize: 'LETTER'
                    }],
                'columnDefs': [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                'select': {
                    'style': 'multi'
                }
            });

            $(".dt-checkboxes-select-all input[type='checkbox']").click();
        },
        error: function (error) {
            $("div.success-message-history").html(error).show();
        }
    });
}
function OrganizeTitle(filters) {

    var title = " ";
    if (filters != null) {
        for (var i = 0; i < filters.length; i++) {
            if (filters[i].operator == "=") {
                filters[i].operator = "Equals";
            }
            else {
                filters[i].operator = "Contains";
            }

            if (i == 0) {
                title = title.concat(filters[i].fieldName + " " + filters[i].operator + " '" + filters[i].filterText + "' ");
            }
            else {
                title = title.concat(" " + "And " + filters[i].fieldName + " " + filters[i].operator + " '" + filters[i].filterText + "'");
            }

        }
    }
    if (title.length > 80) {
        title = title.substring(0, 80) + "...";
    }
    return title;
}
function updateCustomers() {

    customers = [];
    $("#customerListTable tbody input[type='checkbox']").each(function () {
        if ($(this).is(':checked') == true) {
            customers.push($(this).parents('tr').data('backofficecustomerid'));
        }
    })
    setSelectedCustomers(customers.length);
}
function getMessageData() {
    $.ajax({
        url: '/MessageHistory/_SearchMessagePartial',
        type: 'POST',
        data: {},
        success: function (result) {
            $('#messageListModel .modal-body').html(result);
        },
        error: function (error) {
            $("div.success-message-history").html(error).show();
        }
    });
}



function selectMessage() {
    var messageId = $('input[name="rdMessage"]:checked').val();
    $.ajax({
        url: '/MessageHistory/GetById',
        type: 'GET',
        data: { Id: parseInt(messageId) },
        success: function (result) {

            $('#txtMessaeTitle').val(result.title);
            $('#subject').val(result.subject);
            $('#msg-wrapper .Editor-editor').html(result.body);
            $('input:radio[name="radioDocumentupload"][value="' + result.documentType + '"]').prop('checked', true);
            if (result.documentType == "fromdrive") {
                $('#thumbnil').attr('src', '/ConversationFile/' + result.fileUpload).show();
            }
            else if (result.documentType == "fromlibary") {
                $('#thumbnil').attr('src', '/DocumentFile/' + result.fileUpload);
            }
            CheckBoxhideshowUpload(result.documentType);
            var filters = result.filters;
            allFilters = filters;
            $.each(JSON.parse(result.filters), function (key, value) {
                $(".filterFieldList").eq(key).val(value.fieldName);
                $(".filterOperator").eq(key).val(value.operator);
                $(".filterText").eq(key).val(value.filterText);
            });
        },
        error: function (error) {
        }
    });
}

function getDocuments() {
    $.ajax({
        url: '/Library/partialDocument',
        type: 'GET',
        data: {},
        success: function (result) {
            $('#mediaModel .modal-body').html(result);
        },
        error: function (error) {
            $("div.success-message-history").html(error).show();
        }
    });
}