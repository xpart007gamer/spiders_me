﻿var payButton = document.querySelector('#paybutton');
var tokenizeButton = document.querySelector('#tokenizebutton');
var WebPaymentsToken = $("#hdnWebPaymentsToken").val();

// Init FattMerchant API 
//var fattJs = new FattJs('SRC-Inc-03464d66bde7', {
var fattJs = new FattJs(WebPaymentsToken, {
    number: {
        id: 'fattjs-number',
        placeholder: '0000 0000 0000 0000',
        style: 'width: 90%; height:90%; border-radius: 3px; border: 1px solid #ccc; padding: .5em .5em; font-size: 91%;'
    },
    cvv: {
        id: 'fattjs-cvv',
        placeholder: '000',
        style: 'width: 30px; height:90%; border-radius: 3px; border: 1px solid #ccc; padding: .5em .5em; font-size: 91%;'
    }
});

// tell fattJs to load in the card fields
fattJs.showCardForm().then(handler => {
    console.log('form loaded');
   
    // for testing!
    handler.setTestPan('');
    handler.setTestCvv('');
    var form = document.querySelector('form');
    form.querySelector('input[name=month]').value = '';
    form.querySelector('input[name=year]').value = '';
    form.querySelector('input[name=cardholder-first-name]').value = '';
    form.querySelector('input[name=cardholder-last-name]').value = '';
    form.querySelector('input[name=phone]').value = '';
})
    .catch(err => {
        console.log('error init form ' + err);
        // reinit form
    });

fattJs.on('card_form_complete', (message) => {
    // activate pay button
    payButton.disabled = false;
    tokenizeButton.disabled = false;
    console.log(message);
});

fattJs.on('card_form_uncomplete', (message) => {
    // deactivate pay button
    payButton.disabled = true;
    tokenizeButton.disabled = true;
    console.log(message);
});



document.querySelector('#tokenizebutton').onclick = () => {
   
    var form = document.querySelector('#form1');
    var url = $("#hdnCreditWalletListUrl").val();

    var walletInfoes = new Object();
    walletInfoes.CreditCardNickName = $("#creditcardnickname").val().trim();
    walletInfoes.CreditCardHolderName = $("#cardholder-first-name").val().trim();
    walletInfoes.CreditCardNumber = $($('input[name="card_number"]').val()).val();
    walletInfoes.CvvNumber = $("#fattjs-cvv").val();

    walletInfoes.Month = $("#month").val();
    walletInfoes.Year = $("#year").val();
    walletInfoes.ServiceAccountNumber = $("#hdnServiceAccountNumber").val();

    if (walletInfoes.CreditCardNickName == "") {

        $("div.createfailure0").show();
        $("div.createfailure0").fadeIn(300).delay(1500).fadeOut(2000);
        return false;

    }
    if (walletInfoes.CreditCardHolderName == "") {

        $("div.createfailure").show();
        $("div.createfailure").fadeIn(300).delay(1500).fadeOut(2000);
        return false;

    }
    var firstName = form.querySelector('input[name=cardholder-first-name]').value.trim();
    var middleName = form.querySelector('input[name=cardholder-first-name]').value.split(' ').slice(0, -1).join(' ').trim();
    var lastName = form.querySelector('input[name=cardholder-first-name]').value.split(' ').slice(-1).join(' ').trim();
    if (firstName == "" || firstName == undefined) {

        $("div.createfailuref").show();
        $("div.createfailuref").fadeIn(300).delay(1500).fadeOut(2000);
        return false;
    }
    if (middleName == "" || middleName == undefined) {

        $("div.createfailureL").show();
        $("div.createfailureL").fadeIn(300).delay(1500).fadeOut(2000);
        return false;
    }
    if (lastName == "" || lastName == undefined) {

        $("div.createfailureL").show();
        $("div.createfailureL").fadeIn(300).delay(1500).fadeOut(2000);
        return false;
    }

    
    else if (walletInfoes.Month == "") {

        $("div.createfailure4").show();
        $("div.createfailure4").fadeIn(300).delay(1500).fadeOut(2000);
    }
    else if (walletInfoes.Year == "") {

        $("div.createfailure5").show();
        $("div.createfailure5").fadeIn(300).delay(1500).fadeOut(2000);
    }
    
    else {
        $.ajax({
            url: url,
            type: "POST",
            dataType: "Json",
            async: true,
            data: { walletInfoes },
            success: function (response) {
                if (response != null) {
                 
                   

                    var successElement = document.querySelector('.success');
                    var errorElement = document.querySelector('.error');
                    var loaderElement = document.querySelector('.loader');

                    successElement.classList.remove('visible');
                    errorElement.classList.remove('visible');
                    loaderElement.classList.add('visible');

                    var form = document.querySelector('#form1');

                    var firstName = form.querySelector('input[name=cardholder-first-name]').value.split(' ').slice(0, -1).join(' ');
                    var lastName = form.querySelector('input[name=cardholder-first-name]').value.split(' ').slice(-1).join(' ');
                    var extraDetails = {
                        total: 1, // 1$
                        firstname: firstName,
                        lastname: lastName,
                        month: form.querySelector('input[name=month]').value,
                        year: form.querySelector('input[name=year]').value,
                        //phone: form.querySelector('input[name=phone]').value,
                        address_1: response.customer.billingAddress1, // customer address line 1
                        address_2: response.customer.billingAddress2, // customer address line 2
                        address_city: response.customer.billingCity, // customer address city
                        address_state: response.customer.billingState, // customer address state
                        address_zip: response.customer.billingZip, // customer address zip
                        address_country: "USA", // customer address country
                        customer_id: response.fattMerchantCustId, // OPTIONAL customer_id -
                        url: "https://omni.fattmerchant.com/#/bill/",
                        method: 'card',

                        validate: false,
                    };

                    // call tokenize api
                    fattJs.tokenize(extraDetails).then((result) => {
                        console.log('tokenize:');
                        console.log(result);
                        if (result) {
                            successElement.querySelector('.token').textContent = result.id;
                            successElement.classList.add('visible');
                        }
                        loaderElement.classList.remove('visible');
                       
                        payementId = result.id;
                        var count = response.walletList.length;
                        var walletInfoId = response.walletList[count - 1].walletInfoID;
                        var customerId = response.customer.customerID;
                        var walletDetails = new Object();
                        walletDetails.WalletInfoId = walletInfoId;
                        walletDetails.CustomerID = customerId;
                        walletDetails.PaymentMethodID = payementId;
                        walletDetails.CreditCardNumber = result.card_last_four;
                        var url = $('#hdnUpdateWalletPaymentUrl').val();

                        $.ajax({
                            url: url,
                            type: 'POST',
                            dataType: 'json',
                            async: true,
                            data: walletDetails,
                            success: function (data, textStatus, xhr) {
                                $('#WalletListCredit').dataTable({
                                    data: data.walletInfosCredit,
                                    "columns": [

                                        {
                                            "render": function (data, type, full, meta) {
                                                return '<input type="radio" id="radiobtn" name="touchbutton" value="' + full.walletInfoID + '" onchange="getWalletId(this)">';
                                            }
                                        },

                                        { "data": "creditCardHolderName" },
                                        { "data": "creditCardNumber" },
                                        { "data": "month" },
                                        { "data": "year" },

                                        {
                                            "render": function (data, type, full, meta) {
                                                return ' <a onclick="deletepayrecord(' + full.walletInfoID + ');" id="walletDelete" style="cursor:pointer;"><span class="fa fa-trash"></span></a>';
                                            }
                                        }
                                    ],

                                    language: {
                                        "search": '<i class="fa fa-search"></i>',
                                        searchPlaceholder: "Search records",
                                        paginate: {
                                            next: '<i class="fa fa-angle-right"></i>',
                                            previous: '<i class="fa fa-angle-left"></i>'
                                        }
                                    },
                                    "lengthChange": false,
                                    "paging": true,
                                    "info": true,
                                    "iDisplayLength": 3,
                                    "bDestroy": true
                                });
                            },
                            error: function (xhr, textStatus, errorThrown) {
                                console.log('Error in Operation');
                            }
                        });

                        $('#create_wallet_Credit').modal('hide');
                        $('#wallet_list').modal('show');

                        $("#WalletInfo_CreditCardHolderName").val('');
                        $("#WalletInfo_CreditCardNumber").val('');
                        $("#WalletInfo_CvvNumber").val('');

                    })
                        .catch(err => {
                           
                            alert(err.message);
                            var count = response.walletList.length;
                            var walletId = response.walletList[count - 1].walletInfoID;

                            var urlD = $('#hdnDeleteWallet').val();

                            $.ajax({
                                type: "Post",
                                url: urlD,
                                data: { walletId: walletId },
                                success: function (data, textStatus, xhr) {
                                    console.log(data);
                                },
                                error: function (xhr, textStatus, errorThrown) {
                                    console.log('Error in Operation');
                                }
                            });
                            errorElement.textContent = err.message;
                            errorElement.classList.add('visible');
                            loaderElement.classList.remove('visible');
                        });
                   


                }
                else {
                    $('#create_wallet_Credit').modal('hide');
                    $('#wallet_list').modal('show');
                    $("div.failures").show();
                    $("div.failures").fadeIn(300).delay(1500).fadeOut(4000);
                }
            },
            failure: function (response) {
                alert(response.responseText);
            },
            error: function (response) {
                alert(response.responseText);
            }
        });
    }
}