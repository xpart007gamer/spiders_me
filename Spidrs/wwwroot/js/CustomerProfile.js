﻿$(document).ready(function () {
    var tblAutopay = $('#tblAutopay').DataTable({
        "searching": false,
        "lengthChange": false,
        "paging": true,
        "info": true,
        "order": []
        //columnDefs: [
        //    { orderable: false, targets: 2 },
        //    { orderable: false, targets: 3 }
        //]

    });

    $('#tblAutopay tbody').on('click', '.btn12', function (e) {

        var data = tblAutopay.row($(this).closest('tr')).data();
        var CustomerEmail = $("#hdnCustomerEmail").val();
        var PaymentDeductDays = $(this).closest('tr').find('#hdntermscondition').val();
        var accountno = data[1].split('<span style="font-size:11px">');
        $("#hdnAccountNumber").val(accountno[0]);

        $("#IsEmailBillings").val($(this).closest('tr').find('#item_IsEmailBilling').prop('checked'));
        $("#IsEmailNotices").val($(this).closest('tr').find('#item_IsEmailNotice').prop('checked'));
        $("#IsEmailPaymentss").val($(this).closest('tr').find('#item_IsEmailPayments').prop('checked'));
        $("#item_IsEmailPaymentssettled").val($(this).closest('tr').find('#item_IsEmailPaymentssettled').prop('checked'));
        $("#hautopayid").val($(this).closest('tr').find('#hautopayid').val());
        $("#hwaletid").val($(this).closest('tr').find('#hwaletid').val());
        var ServiceAccountNumber = $(this).closest('tr').find('#hServiceAccountNumbers').val();
        var CustomerServiceId = $(this).closest('tr').find('#hCustomerServiceId').val();
        $("#hdnServiceAccountNumber").val(ServiceAccountNumber);
        var urlGetPendingAmount = $("#hdnGetPendingAmount").val();

        $.ajax({
            type: "Post",
            url: urlGetPendingAmount,
            data: { CustomerServiceId: CustomerServiceId },
            success: function (result) {

                if (result == 1) {
                    $("div.pendingamttosetup").show();
                    $("div.pendingamttosetup").fadeIn(300).delay(1500).fadeOut(4000);

                }
                else {

                    var url = $("#hdnWalletListUrl").val();
                    $.ajax({
                        type: "GET",
                        url: url,
                        data: { ServiceAccountNumber: ServiceAccountNumber },
                        contentType: "application/json; charset=utf-8",
                        dataType: "Json",
                        success: function (response) {

                            $("label[for='lblAccount']").text(accountno[0]);
                            $("label[for='lblemail']").text(CustomerEmail);
                            if (PaymentDeductDays == 0) {
                                $("#Afterduedate").val(7);
                            }
                            else {
                                $("#Afterduedate").val(PaymentDeductDays);
                            }
                            $('#WalletList').dataTable({
                                data: response.walletInfos,
                                "columns": [
                                    {
                                        "render": function (data, type, full, meta) {

                                            if ($("#hwaletid").val() == full.walletInfoID) {
                                                $("#ach").prop("checked", true);
                                                $('#achdiv').css('display', 'block')
                                                $('#WalletListdiv').css('display', 'block')
                                                $('#creditdiv').css('display', 'none')
                                                $('#WalletListCreditdiv').css('display', 'none')
                                                getWalletIdinfo(full.walletInfoID)
                                                $('#checkboxterm').prop('checked', true);
                                                return '<input type="radio" checked="checked"  id="radiobtn" name="touchbutton" value="' + full.walletInfoID + '" onchange="getWalletId(this)">';
                                            }
                                            else {
                                                return '<input type="radio" id="radiobtn" name="touchbutton" value="' + full.walletInfoID + '" onchange="getWalletId(this)">';
                                            }

                                        }
                                    },
                                    { "data": "bankName" },

                                    { "data": "bankAccountNumber" },

                                    {
                                        "render": function (data, type, full, meta) {
                                            return ' <a onclick="deletepayrecord(' + full.walletInfoID + ');" id="walletDelete" style="cursor:pointer;"><span class="fa fa-trash"></span></a>';
                                        }
                                    }
                                ],
                                language: {
                                    "search": '<i class="fa fa-search"></i>',
                                    searchPlaceholder: "Search records",
                                    paginate: {
                                        next: '<i class="fa fa-angle-right"></i>',
                                        previous: '<i class="fa fa-angle-left"></i>'
                                    }
                                },

                                "lengthChange": false,
                                "paging": true,
                                "info": true,
                                "iDisplayLength": 3,
                                "bDestroy": true

                            });


                            $('#WalletListCredit').dataTable({
                                data: response.walletInfosCredit,

                                "columns": [
                                    {
                                        "render": function (data, type, full, meta) {

                                            if ($("#hwaletid").val() == full.walletInfoID) {
                                                getWalletIdinfo(full.walletInfoID)
                                                $('#checkboxtermcredit').prop('checked', true);

                                                $("#credit").prop("checked", true);
                                                $('#creditdiv').css('display', 'block')
                                                $('#achdiv').css('display', 'none')
                                                $('#WalletListdiv').css('display', 'none')
                                                $('#WalletListCreditdiv').css('display', 'block')
                                                return '<input type="radio" checked="checked"  id="radiobtn" name="touchbutton" value="' + full.walletInfoID + '" onchange="getWalletId(this)">';
                                            }
                                            else {
                                                return '<input type="radio" id="radiobtn" name="touchbutton" value="' + full.walletInfoID + '" onchange="getWalletId(this)">';
                                            }

                                        }
                                    },
                                    { "data": "creditCardHolderName" },
                                    { "data": "creditCardNumber" },
                                    { "data": "month" },
                                    { "data": "year" },
                                    {
                                        "render": function (data, type, full, meta) {
                                            return ' <a onclick="deletepayrecord(' + full.walletInfoID + ');" id="walletDelete" style="cursor:pointer;"><span class="fa fa-trash"></span></a>';
                                        }
                                    }


                                ],
                                language: {
                                    "search": '<i class="fa fa-search"></i>',
                                    searchPlaceholder: "Search records",
                                    paginate: {
                                        next: '<i class="fa fa-angle-right"></i>',
                                        previous: '<i class="fa fa-angle-left"></i>'
                                    }
                                },

                                "lengthChange": false,
                                "paging": true,
                                "info": true,
                                "iDisplayLength": 3,
                                "bDestroy": true

                            });

                            $('#wallet_list').modal('show');
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        },
                        error: function (response) {
                            alert(response.responseText);
                        }
                    });
                }
            },
            error: function (response) {
            }
        });



    });


    $('#tblAutopay tbody').on('click', '.stopautopay', function () {

        var data = tblAutopay.row($(this).closest('tr')).data();
        var accountnumber = data[1];
        $("#cuid").val(accountnumber);
        $('#myModal45656').modal('show');

        //var checkstr = confirm('Are you sure you want to delete Auto Pay for this account?');
        //if (checkstr == true) {


        //    var url = $("#hdndeleteAutoPayUrl").val();

        //    $.ajax({
        //        type: "Post",
        //        url: url,
        //        data: { accountnumber: accountnumber },
        //        success: function (result) {
        //            $("div.deleteautosetup").show();
        //            $("div.deleteautosetup").fadeIn(300).delay(1500).fadeOut(4000);
        //            setTimeout(function () {
        //                var url = $("#hdnGetCustomerProfileUrl").val();
        //                window.location.href = url;
        //            }, 3000);
        //        }
        //    })
        //}
        //else {
        //    return false;
        //}

    });

    //none case all checkboxses blank
    $('#tblAutopay tbody').on('click', '#nonecheck', function (e) {

        if ($(this).prop('checked')) {
            $(this).closest('tr').find('#item_IsEmailBilling').prop('checked', false);
            $(this).closest('tr').find('#item_IsEmailNotice').prop('checked', false);
            $(this).closest('tr').find('#item_IsEmailPaymentssettled').prop('checked', false);
            $(this).closest('tr').find('#item_IsEmailPayments').prop('checked', false);
        }
    });

    $('#tblAutopay tbody').on('click', '.unchekall', function (e) {

        if ($(this).prop('checked')) {
            $(this).closest('tr').find('#nonecheck').prop('checked', false);
        }
    });
});


$('#btnSave').on('click', function () {

    var WebPaymentsToken = $("#hdnWebPaymentsToken").val();
    var url = $("#hdnCreateWalletListUrl").val();
    var walletInfoes = new Object();
    walletInfoes.BankName = $("#WalletInfo_BankName").val();
    walletInfoes.BankAccountNumber = $("#WalletInfo_BankAccountNumber").val();
    walletInfoes.BankType = $("#ddlBankType").val();
    walletInfoes.BankHolderType = $("#ddlBankHolderType").val();
    walletInfoes.BankRoutingNumber = $("#WalletInfo_BankRoutingNumber").val();
    walletInfoes.ServiceAccountNumber = $("#hdnServiceAccountNumber").val();
    if (walletInfoes.BankName == "") {

        $("div.createfailure").show();
        $("div.createfailure").fadeIn(300).delay(1500).fadeOut(2000);
    }

    else if (walletInfoes.BankRoutingNumber == "") {

        $("div.createfailure1").show();
        $("div.createfailure1").fadeIn(300).delay(1500).fadeOut(2000);
    }
    else if (walletInfoes.BankAccountNumber == "") {

        $("div.createfailure2").show();
        $("div.createfailure2").fadeIn(300).delay(1500).fadeOut(2000);
    }
    else {
        $.ajax({
            url: url,
            type: "POST",
            dataType: "Json",
            async: true,
            data: { walletInfoes },
            success: function (response) {
                if (response != null) {

                    var personName = response.customer.firstName + " " + response.customer.lastName;
                    var walletDetails = {
                        /* Start Bank Details */
                        method: "bank", // very important to set this as "bank" for ACH payments
                        bank_name: walletInfoes.BankName, // bank name, e.g. "Chase"
                        bank_account: walletInfoes.BankAccountNumber, // bank account number
                        bank_routing: walletInfoes.BankRoutingNumber, // bank routing number
                        bank_type: walletInfoes.BankType, // "checking" or "savings"
                        bank_holder_type: walletInfoes.BankHolderType, // "personal" or "business"
                        /* End Bank Details */

                        person_name: personName,
                        phone: response.customer.phone,
                        address_1: response.customer.billingAddress1, // customer address line 1
                        address_2: response.customer.billingAddress2, // customer address line 2
                        address_city: response.customer.billingCity, // customer address city
                        address_state: response.customer.billingState, // customer address state
                        address_zip: response.customer.billingZip, // customer address zip
                        address_country: "USA", // customer address country
                        customer_id: response.fattMerchantCustId, // OPTIONAL customer_id -

                        validate: false,
                    };

                    var payementId;
                    // making our instance of fattjs
                    //var fattJs = new FattJs("SRC-Inc-03464d66bde7", {
                    var fattJs = new FattJs(WebPaymentsToken, {
                    });

                    // call tokenize api
                    fattJs
                        .tokenize(walletDetails)
                        .then((tokenizedPaymentMethod) => {
                            // tokenizedPaymentMethod is the tokenized payment record
                            console.log('successful tokenization:', tokenizedPaymentMethod);
                            payementId = tokenizedPaymentMethod.id;
                            var count = response.walletList.length;
                            var walletInfoId = response.walletList[count - 1].walletInfoID;
                            var customerId = response.customer.customerID;
                            var walletDetails = new Object();
                            walletDetails.WalletInfoId = walletInfoId;
                            walletDetails.CustomerID = customerId;
                            walletDetails.PaymentMethodID = payementId;
                            var url = $('#hdnUpdateWalletPaymentUrl').val();

                            $.ajax({
                                url: url,
                                type: 'POST',
                                dataType: 'json',
                                async: true,
                                data: walletDetails,
                                success: function (data, textStatus, xhr) {
                                    console.log(data);
                                },
                                error: function (xhr, textStatus, errorThrown) {
                                    console.log('Error in Operation');
                                }
                            });


                            $('#WalletList').dataTable({
                                data: response.walletList,
                                "columns": [

                                    {
                                        "render": function (data, type, full, meta) {
                                            return '<input type="radio" id="radiobtn" name="touchbutton" value="' + full.walletInfoID + '" onchange="getWalletId(this)">';
                                        }
                                    },

                                    { "data": "bankName" },
                                    { "data": "bankAccountNumber" },

                                    {
                                        "render": function (data, type, full, meta) {
                                            return ' <a onclick="deletepayrecord(' + full.walletInfoID + ');" id="walletDelete" style="cursor:pointer;"><span class="fa fa-trash"></span></a>';
                                        }
                                    }

                                ],
                                "aoColumnDefs": [
                                    { "bSortable": false, "aTargets": [3] },
                                    { "bSearchable": false, "aTargets": [3] }
                                ],

                                language: {
                                    "search": '<i class="fa fa-search"></i>',
                                    searchPlaceholder: "Search records",
                                    paginate: {
                                        next: '<i class="fa fa-angle-right"></i>',
                                        previous: '<i class="fa fa-angle-left"></i>'
                                    }
                                },

                                "searching": true,
                                "lengthChange": false,
                                "paging": true,
                                "info": true,
                                "iDisplayLength": 3,
                                "bDestroy": true

                            });

                            $('#create_wallet').modal('hide');
                            $('#wallet_list').modal('show');


                        })
                        .catch(err => {

                            alert(err["errors"]);

                            var count = response.walletList.length;
                            var walletId = response.walletList[count - 1].walletInfoID;

                            var urlD = $('#hdnDeleteWallet').val();

                            $.ajax({
                                type: "Post",
                                url: urlD,
                                data: { walletId: walletId },
                                success: function (data, textStatus, xhr) {
                                    console.log(data);
                                },
                                error: function (xhr, textStatus, errorThrown) {
                                    console.log('Error in Operation');
                                }
                            });
                            // handle errors here
                            console.log('unsuccessful tokenization:', err);
                        });




                    $("#WalletInfo_BankName").val('');
                    $("#WalletInfo_BankAccountNumber").val('');
                    $("#WalletInfo_BankType").val('');
                    $("#WalletInfo_BankHolderType").val('');
                    $("#WalletInfo_BankRoutingNumber").val('');
                }
                else {
                    $('#create_wallet').modal('hide');
                    $('#wallet_list').modal('show');
                    $("div.failures").show();
                    $("div.failures").fadeIn(300).delay(1500).fadeOut(4000);
                }
            },
            failure: function (response) {
                alert(response.responseText);
            },
            error: function (response) {
                alert(response.responseText);
            }
        });
    }

});

$('#btnSaveCredit').on('click', function () {
    var WebPaymentsToken = $("#hdnWebPaymentsToken").val();
    var url = $("#hdnCreditWalletListUrl").val();
    var walletInfoes = new Object();
    walletInfoes.CreditCardHolderName = $("#WalletInfo_CreditCardHolderName").val();
    walletInfoes.CreditCardNumber = $("#WalletInfo_CreditCardNumber").val();
    walletInfoes.CvvNumber = $("#WalletInfo_CvvNumber").val();

    walletInfoes.Month = $("#monthid option:selected").val();
    walletInfoes.Year = $("#yearid option:selected").val();

    if (walletInfoes.CreditCardHolderName == "") {

        $("div.createfailure").show();
        $("div.createfailure").fadeIn(300).delay(1500).fadeOut(2000);
    }

    else if (walletInfoes.CreditCardNumber == "") {

        $("div.createfailure1").show();
        $("div.createfailure1").fadeIn(300).delay(1500).fadeOut(2000);
    }
    else if (walletInfoes.CvvNumber == "") {

        $("div.createfailure2").show();
        $("div.createfailure2").fadeIn(300).delay(1500).fadeOut(2000);
    }
    else if (walletInfoes.Month == "") {

        $("div.createfailure4").show();
        $("div.createfailure4").fadeIn(300).delay(1500).fadeOut(2000);
    }
    else if (walletInfoes.Year == "") {

        $("div.createfailure5").show();
        $("div.createfailure5").fadeIn(300).delay(1500).fadeOut(2000);
    }
    else {
        $.ajax({
            url: url,
            type: "POST",
            dataType: "Json",
            async: true,
            data: { walletInfoes },
            success: function (response) {

                if (response != null) {
                    $('#WalletListCredit').dataTable({
                        data: response.walletList,
                        "columns": [
                            {
                                "render": function (data, type, full, meta) {
                                    return '<input type="radio" id="radiobtn" name="touchbutton" value="' + full.walletInfoID + '" onchange="getWalletId(this)">';
                                }
                            },
                            { "data": "creditCardHolderName" },
                            { "data": "creditCardNumber" },
                            { "data": "month" },
                            { "data": "year" },

                        ],

                        language: {
                            "search": '<i class="fa fa-search"></i>',
                            searchPlaceholder: "Search records",
                            paginate: {
                                next: '<i class="fa fa-angle-right"></i>',
                                previous: '<i class="fa fa-angle-left"></i>'
                            }
                        },
                        "lengthChange": false,
                        "paging": true,
                        "info": true,
                        "iDisplayLength": 3,
                        "bDestroy": true
                    });

                    var personName = response.customer.firstName + " " + response.customer.lastName;
                    var extraDetails = {
                        total: 10, // $10.00
                        firstname: response.customer.firstName, // customer first name
                        lastname: response.customer.lastName, // customer last name
                        company: "", // customer company
                        email: "", // customer email - receipt will be sent automatically
                        month: walletInfoes.Month, // credit card expiration month
                        year: walletInfoes.Year, // credit card expiration year
                        phone: "5555555555", // customer phone number
                        address_1: response.customer.billingAddress1, // customer address line 1
                        address_2: response.customer.billingAddress2, // customer address line 2
                        address_city: response.customer.billingCity, // customer address city
                        address_state: response.customer.billingState, // customer address state
                        address_zip: response.customer.billingZip, // customer address zip
                        address_country: "USA", // customer address country
                        customer_id: response.fattMerchantCustId, // OPTIONAL customer_id -
                        validate: false,
                    };

                    // call pay api
                    var payementId;
                    // making our instance of fattjs
                    //var fattJs = new FattJs("SRC-Inc-03464d66bde7", {
                    var fattJs = new FattJs(WebPaymentsToken, {
                    });

                    // call tokenize api
                    fattJs
                        .tokenize(extraDetails)
                        .then((tokenizedPaymentMethod) => {
                            // tokenizedPaymentMethod is the tokenized payment record
                            console.log('successful tokenization:', tokenizedPaymentMethod);
                            payementId = tokenizedPaymentMethod.id;
                            var count = response.walletList.length;
                            var walletInfoId = response.walletList[count - 1].walletInfoID;
                            var customerId = response.customer.customerID;
                            var walletDetails = new Object();
                            walletDetails.WalletInfoId = walletInfoId;
                            walletDetails.CustomerID = customerId;
                            walletDetails.PaymentMethodID = payementId;
                            var url = $('#hdnUpdateWalletPaymentUrl').val();
                            $.ajax({
                                url: url,
                                type: 'POST',
                                dataType: 'json',
                                async: true,
                                data: walletDetails,
                                success: function (data, textStatus, xhr) {
                                    console.log(data);
                                },
                                error: function (xhr, textStatus, errorThrown) {
                                    console.log('Error in Operation');
                                }
                            });

                        })
                        .catch(err => {
                            // handle errors here
                            console.log('unsuccessful tokenization:', err);
                        });
                    $('#create_wallet_Credit').modal('hide');
                    $('#wallet_list').modal('show');

                    $("#WalletInfo_CreditCardHolderName").val('');
                    $("#WalletInfo_CreditCardNumber").val('');
                    $("#WalletInfo_CvvNumber").val('');

                }
                else {
                    $('#create_wallet_Credit').modal('hide');
                    $('#wallet_list').modal('show');
                    $("div.failures").show();
                    $("div.failures").fadeIn(300).delay(1500).fadeOut(4000);
                }
            },
            failure: function (response) {
                alert(response.responseText);
            },
            error: function (response) {
                alert(response.responseText);
            }
        });
    }

});

///Payment paynow
$('#btnPaynow').on('click', function (e) {

    var autoPayment = new Object();
    autoPayment.WalletID = $("#hdnWalletId").val();
    autoPayment.ServiceAccountNumber = $("#hdnAccountNumber").val();
    autoPayment.EmailID = $("#hdnCustomerEmail").val();
    autoPayment.CustomerID = $("#hdnCustomerId").val();
    autoPayment.IsEmailBilling = $("#IsEmailBillings").val();
    autoPayment.IsEmailNotice = $("#IsEmailNotices").val();
    autoPayment.IsEmailPayments = $("#IsEmailPaymentss").val();
    autoPayment.IsEmailPaymentssettled = $("#IsEmailPaymentssettled").val();
    autoPayment.None = $("#IsEmailPaymentss").val();
    autoPayment.TermsCondition = $("#checkboxterm").prop('checked');
    autoPayment.PaymentDeductDays = $("#Afterduedate").val();
    var url = $("#hdnAutoPayAccountListUrl").val();

    if (autoPayment.WalletID == "") {
        $("div.selectfailures").show();
        $("div.selectfailures").fadeIn(300).delay(1500).fadeOut(3000);
    }
    else if ($("#checkboxterm").prop('checked') != true) {
        $("div.failures").show();
        $("div.failures").fadeIn(300).delay(1500).fadeOut(3000);
    }
    else if (autoPayment.PaymentDeductDays == "0" || autoPayment.PaymentDeductDays == "1" || autoPayment.PaymentDeductDays == "") {

        $("div.selectPaymentDeductDays").show();
        $("div.selectPaymentDeductDays").fadeIn(300).delay(1500).fadeOut(3000);
    }

    else {
        $.ajax({
            url: url,
            type: 'POST',
            async: true,
            dataType: 'json',
            data: { autoPayment },
            success: function (data) {
                if (data > 0) {
                    $('#wallet_list').modal('hide');
                    $("div.success").show();
                    $("div.success").fadeIn(300).delay(1500).fadeOut(4000);
                    setTimeout(function () {
                        var url = $("#hdnGetCustomerProfileUrl").val();
                        window.location.href = url;
                    }, 3000);
                }
                else if (data == 0) {
                    $('#wallet_list').modal('hide');
                    $("div.Updatesuccess").show();
                    $("div.Updatesuccess").fadeIn(300).delay(1500).fadeOut(4000);
                    setTimeout(function () {
                        var url = $("#hdnGetCustomerProfileUrl").val();
                        window.location.href = url;
                    }, 3000);
                }
                else {
                    $("div.failure").show();
                    $("div.failure").fadeIn(300).delay(1500).fadeOut(4000);
                }

            },
            error: function (xhr, textStatus, errorThrown) {
                console.log('Error in Operation');
            }
        });

    }
});

///Payment paynow Credit card
$('#btnPaynowcredit').on('click', function (e) {

    var autoPayment = new Object();
    autoPayment.WalletID = $("#hdnWalletId").val();
    autoPayment.ServiceAccountNumber = $("#hdnAccountNumber").val();
    autoPayment.EmailID = $("#hdnCustomerEmail").val();
    autoPayment.CustomerID = $("#hdnCustomerId").val();
    autoPayment.IsEmailBilling = $("#IsEmailBillings").val();
    autoPayment.IsEmailNotice = $("#IsEmailNotices").val();
    autoPayment.IsEmailPayments = $("#IsEmailPaymentss").val();
    autoPayment.IsEmailPaymentssettled = $("#IsEmailPaymentssettled").val();
    autoPayment.None = $("#IsEmailPaymentss").val();
    autoPayment.TermsCondition = $("#checkboxtermcredit").prop('checked');
    autoPayment.PaymentDeductDays = $("#Afterduedate").val();
    var url = $("#hdnAutoPayAccountListUrl").val();

    if (autoPayment.WalletID == "") {
        $("div.selectfailures").show();
        $("div.selectfailures").fadeIn(300).delay(1500).fadeOut(3000);
    }
    else if ($("#checkboxtermcredit").prop('checked') != true) {
        $("div.failures").show();
        $("div.failures").fadeIn(300).delay(1500).fadeOut(3000);
    }
    else if (autoPayment.PaymentDeductDays == "0" || autoPayment.PaymentDeductDays == "1" || autoPayment.PaymentDeductDays == "") {
        $("div.selectPaymentDeductDays").show();
        $("div.selectPaymentDeductDays").fadeIn(300).delay(1500).fadeOut(3000);
    }

    else {
        $.ajax({
            url: url,
            type: 'POST',
            async: true,
            dataType: 'json',
            data: { autoPayment },
            success: function (data) {
                if (data > 0) {
                    $('#wallet_list').modal('hide');
                    $("div.success").show();
                    $("div.success").fadeIn(300).delay(1500).fadeOut(4000);
                    setTimeout(function () {
                        var url = $("#hdnGetCustomerProfileUrl").val();
                        window.location.href = url;
                    }, 3000);
                }
                else if (data == 0) {
                    $('#wallet_list').modal('hide');
                    $("div.Updatesuccess").show();
                    $("div.Updatesuccess").fadeIn(300).delay(1500).fadeOut(4000);
                    setTimeout(function () {
                        var url = $("#hdnGetCustomerProfileUrl").val();
                        window.location.href = url;
                    }, 3000);
                }
                else {
                    $("div.failure").show();
                    $("div.failure").fadeIn(300).delay(1500).fadeOut(4000);
                }

            },
            error: function (xhr, textStatus, errorThrown) {
                console.log('Error in Operation');
            }
        });

    }
});


//Numeric input validation
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

//Only numeric  can be paste
function process(input) {
    let value = input.value;
    let numbers = value.replace(/[^0-9]/g, "");
    input.value = numbers;
}

function getWalletId($this) {

    $("#hdnWalletId").val($this.value);
}

function getWalletIdinfo(walletId) {
    $("#hdnWalletId").val(walletId);
}

///Phone 1 update 
function Phone1() {
    var allVals = [];
    $('#c_b :checked').each(function () {
        allVals.push($(this).val());
    });
    var status = allVals[0];
    if (status == "true") {
        status = 1;
    }
    else {
        status = 0
    }
    var url = $("#hdnphone1url").val();
    $.ajax({
        type: "Post",
        url: url,
        data: { status: status },
        success: function (result) {

            if (result == true) {
                $("div.pHONE1").show();
                $("div.pHONE1").fadeIn(400).delay(1600).fadeOut(3000);
                setTimeout(function () {
                    var url = $("#hdnGetCustomerProfileUrl").val();
                    window.location.href = url;
                }, 3000);
            }
            else {
                $("div.pHONEerror").show();
                $("div.pHONEerror").fadeIn(400).delay(1600).fadeOut(3000);
                setTimeout(function () {
                    var url = $("#hdnGetCustomerProfileUrl").val();
                    window.location.href = url;
                }, 3000);
            }

        }
    })
}

function AllowTextPhone1(PhoneType) {
    AllowTextPhone($("#Phone1Text").is(':checked'), PhoneType);
}

function AllowTextPhone2(PhoneType) {
    AllowTextPhone($("#Phone2Text").is(':checked'), PhoneType);
}

function AllowTextPhone(status, PhoneType) {
    if (status == true) {
        status = 1;
    }
    else {
        status = 0
    }
    var url = $("#hdnphoneurl").val();
    $.ajax({
        type: "Post",
        url: url,
        data: { status: status, PhoneType: PhoneType },
        success: function (result) {

            if (result == true) {
                $("div.pHONE1").show();
                $("div.pHONE1").fadeIn(400).delay(1600).fadeOut(3000);
                setTimeout(function () {
                    var url = $("#hdnGetCustomerProfileUrl").val();
                    window.location.href = url;
                }, 3000);
            }
            else {
                $("div.pHONEerror").show();
                $("div.pHONEerror").fadeIn(400).delay(1600).fadeOut(3000);
                setTimeout(function () {
                    var url = $("#hdnGetCustomerProfileUrl").val();
                    window.location.href = url;
                }, 3000);
            }

        }
    })
}

function Allsavedata() {

    var customer = [];
    var formData = new FormData();
    var j = 0;
    $("#tblAutopay TBODY TR").each(function (index, value) {
        var row = $(this);


        var row = $(this);
        customer.CustomerIDs = row.find(".CustomerIDs").val();
        customer.ServiceAccountNumbers = row.find(".ServiceAccountNumbers").val();
        customer.IsEmailBillings = $(this).closest('tr').find('.IsEmailBillings').prop('checked'); //row.find(".IsEmailBillings").val();
        customer.IsEmailPaymentss = $(this).closest('tr').find('.IsEmailPaymentss').prop('checked'); //row.find(".IsEmailPaymentss");
        customer.IsEmailNotices = $(this).closest('tr').find('.IsEmailNotices').prop('checked'); //row.find(".IsEmailNotices");
        customer.IsEmailPaymentssettled = $(this).closest('tr').find('.IsEmailPaymentssettled').prop('checked'); //row.find(".IsEmailNotices");

        formData.append("multidata[" + j + "].CustomerIDs", customer.CustomerIDs);
        formData.append("multidata[" + j + "].ServiceAccountNumbers", customer.ServiceAccountNumbers);
        formData.append("multidata[" + j + "].IsEmailBillings", customer.IsEmailBillings);
        formData.append("multidata[" + j + "].IsEmailPaymentss", customer.IsEmailPaymentss);
        formData.append("multidata[" + j + "].IsEmailNotices", customer.IsEmailNotices);
        formData.append("multidata[" + j + "].IsEmailPaymentssettled", customer.IsEmailPaymentssettled);
        j++;
    });
    var url = $("#hdSaveAuotall").val();
    $.ajax({
        type: "Post",
        url: url,
        contentType: false,
        processData: false,
        data: formData,
        success: function (result) {

            //swal({

            //    text: "Record updated successfully",
            //    icon: "success",
            //})
            //    .then((ok) => {
            //        if (ok) {
            //            var url = $("#hdnGetCustomerProfileUrl").val();
            //           window.location.href = url;

            //        }
            //    });
            $("div.Autosuccess").show();
            $("div.Autosuccess").fadeIn(400).delay(1600).fadeOut(3000);
            setTimeout(function () {
                var url = $("#hdnGetCustomerProfileUrl").val();
                window.location.href = url;
            }, 3000);
        },
        error: function (response) {

        }
    });

}


$("#Afterduedate").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which !== 8 && e.which !== 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg1").html("Please Enter Number only").show().fadeOut(3000);
        return false;
    }
}).keyup(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (parseInt(e.target.value) > 15) {
        $("#errmsg1").html("Maximum 15 days are allowed").show().fadeOut(3000);
        e.target.value = '';
        return false;
    }
});


$("#inline_content input[name='bankradio']").click(function () {
    if ($('input:radio[name=bankradio]:checked').val() == "AchCard") {
        $('#achdiv').css('display', 'block')
        $('#WalletListdiv').css('display', 'block')
        $('#creditdiv').css('display', 'none')
        $('#WalletListCreditdiv').css('display', 'none')
        //jq('#header').css("visibility", "hidden");
        //$('#header').css("visibility", "visible");
    }
    else if ($('input:radio[name=bankradio]:checked').val() == "CreditCard") {
        $('#creditdiv').css('display', 'block')
        $('#achdiv').css('display', 'none')
        $('#WalletListdiv').css('display', 'none')
        $('#WalletListCreditdiv').css('display', 'block')
    }
});
//Stop the Auto pay
var DeleteAuto = function () {

    var accountnumber = $("#cuid").val();
    var url = $("#hdndeleteAutoPayUrl").val();
    $.ajax({
        type: "Post",
        url: url,
        data: { accountnumber: accountnumber },
        success: function (result) {
            $('#myModal45656').modal('hide');
            $("div.deleteautosetup").show();
            $("div.deleteautosetup").fadeIn(300).delay(1500).fadeOut(4000);
            setTimeout(function () {
                var url = $("#hdnGetCustomerProfileUrl").val();
                window.location.href = url;
            }, 3000);
        }
    })
}

function deletepayrecord(hdwalletid) {

    $("#hdwalletid").val(hdwalletid);
    $("#dasmodel").modal("show");
}
var Delete = function () {

    var id = $("#hdwalletid").val();
    var deleteurl = $('#hdndeletewalletrecord').val();
    $.ajax({
        type: "Post",
        url: deleteurl,
        data: { walletInfoId: id },
        success: function (response) {

            $('#WalletList').dataTable({
                data: response.walletInfos,
                "columns": [
                    {
                        "render": function (data, type, full, meta) {
                            return '<input type="radio" id="radiobtn" name="touchbutton" value="' + full.walletInfoID + '" onchange="getWalletId(this)">';
                        }
                    },

                    { "data": "bankName" },
                    { "data": "bankAccountNumber" },

                    {
                        "render": function (data, type, full, meta) {
                            return ' <a onclick="deletepayrecord(' + full.walletInfoID + ');" id="walletDelete" style="cursor:pointer;"><span class="fa fa-trash"></span></a>';
                        }
                    }
                ],
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [3] },
                    { "bSearchable": false, "aTargets": [3] }
                ],

                language: {
                    "search": '<i class="fa fa-search"></i>',
                    searchPlaceholder: "Search records",
                    paginate: {
                        next: '<i class="fa fa-angle-right"></i>',
                        previous: '<i class="fa fa-angle-left"></i>'
                    }
                },

                "searching": true,
                "lengthChange": false,
                "paging": true,
                "info": true,
                "iDisplayLength": 3,
                "bDestroy": true

            });


            $('#WalletListCredit').dataTable({
                data: response.walletInfosCredit,

                "columns": [
                    {
                        "render": function (data, type, full, meta) {
                            if ($("#hwaletid").val() == full.walletInfoID) {

                                return '<input type="radio" checked="checked"  id="radiobtn" name="touchbutton" value="' + full.walletInfoID + '" onchange="getWalletId(this)">';
                            }
                            else {
                                return '<input type="radio" id="radiobtn" name="touchbutton" value="' + full.walletInfoID + '" onchange="getWalletId(this)">';
                            }

                        }
                    },
                    { "data": "creditCardHolderName" },
                    { "data": "creditCardNumber" },
                    { "data": "month" },
                    { "data": "year" },
                    {
                        "render": function (data, type, full, meta) {
                            return ' <a onclick="deletepayrecord(' + full.walletInfoID + ');" id="walletDelete" style="cursor:pointer;"><span class="fa fa-trash"></span></a>';
                        }
                    }


                ],
                language: {
                    "search": '<i class="fa fa-search"></i>',
                    searchPlaceholder: "Search records",
                    paginate: {
                        next: '<i class="fa fa-angle-right"></i>',
                        previous: '<i class="fa fa-angle-left"></i>'
                    }
                },

                "lengthChange": false,
                "paging": true,
                "info": true,
                "iDisplayLength": 3,
                "bDestroy": true

            });
            $("#dasmodel").modal("hide");
            $("div.success").show();
            $("div.success").fadeIn(300).delay(1500).fadeOut(4000);
        }
    })
}