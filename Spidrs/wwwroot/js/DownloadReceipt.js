﻿//PDF DOWNLOAD for receipt view
function downloadreceiptpdf(PaymentID, PaymentTable) {

  
    
    //$(".pdfs").css("opacity", 0.5);
    //$(".pdfs").css("background-color", "A9A9A9");
    $(".loader-wrapper").addClass("show1");
    var pdfurl = $('#hdnDownloadReceiptPrintPDFHtml').val();
  
    $.ajax({
        type: 'POST',
        url: pdfurl,
        datatype: "html",
        data: { PaymentID: PaymentID, PaymentTable: PaymentTable },
        success: function (response) {
          
            var pdfurl2 = $('#hdnDownloadReceiptPrintPD').val();
            if (response != "") {
                $.ajax({
                    type: 'POST',
                    url: pdfurl2,
                    dataType: "JSON",
                    data: { chnageOrderDocument: response },
                    success: function (response) {
                     
                        if (response != "") {
                            var sampleBytes = base64ToArrayBuffer(response.base64);
                            saveByteArray([sampleBytes], response.fileName + '.pdf');
                            //$(".pdfs").css("opacity", "");
                            //$(".pdfs").css("background-color", "");
                            //$('#primg').hide();
                            $(".loader-wrapper").removeClass("show1");
                        }
                    },
                    error: function () {
                        $(".loader-wrapper").removeClass("show1");
                    }
                });
            }
        },
        error: function () {
            $(".loader-wrapper").removeClass("show1");
            swal({

                text: "Something is wrong.",
                icon: "error",

            })
                .then((ok) => {
                    if (ok) {


                    }
                });
        }
    });
}
//});
function base64ToArrayBuffer(base64) {

    var binaryString = window.atob(base64);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
        var ascii = binaryString.charCodeAt(i);
        bytes[i] = ascii;
    }
    return bytes;
}

var saveByteArray = (function () {
    var a = document.createElement("a");
    document.body.appendChild(a);
    //a.style = "display: none";
    return function (data, name) {
        var blob = new Blob(data, { type: "octet/stream" }),
            url = window.URL.createObjectURL(blob);
        a.href = url;
        a.download = name;
        a.click();
        window.URL.revokeObjectURL(url);
    };
}());