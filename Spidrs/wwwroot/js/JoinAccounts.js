﻿$(document).ready(function () {

    $("div.btnparentjoinaccount").hide();
    $("div.btnchildjoinaccount").hide();

    var tblcustomer = $('#tblJoinAccount').DataTable({


        language: {
            "search": '<i class="fa fa-search"></i>',
            searchPlaceholder: "Search records",
            paginate: {
                next: '<i class="fa fa-angle-right"></i>',
                previous: '<i class="fa fa-angle-left"></i>'
            }
        },
        "searching": true,
        "lengthChange": false,
        "paging": true,
        "info": true,
        "order": []

    });
    $(".loader-wrapper").removeClass("show1");
    onLoadbinddata();
});

$(document).ready(function () {

    var tblcustomer = $('#tblJoinAccountList').DataTable({


        language: {
            "search": '<i class="fa fa-search"></i>',
            searchPlaceholder: "Search records",
            paginate: {
                next: '<i class="fa fa-angle-right"></i>',
                previous: '<i class="fa fa-angle-left"></i>'
            }
        },
        "searching": true,
        "lengthChange": false,
        "paging": true,
        "info": true,
        "order": []

    });
    $(".loader-wrapper").removeClass("show1");
    $('div.dataTables_info').parent().css("display", "grid");
    $('#tblJoinAccountList_wrapper div.dataTables_info').parent().append(' <div class="d-flex mt-4 ml-4"><div class= "textbox-wrapper"><input class="form-control w-100 joinAccountToClear" placeholder="Back Office Customer Id" id="joinAccountToClear" /></div><div class="btn-wrapper text-right"> <button class="btn btn-danger ml-4" onclick="Confirm(this)">Clear Join</button></div ></div >');
    SearchText();
});
///AutoComplete Functionality

function SearchText() {
    /// Parent customer autocomplete
    $("#txtCustomer").autocomplete({
        source: function (request, response) {
            var UrlAutoComplete = $('#hdAutoCompleteParent').val();
            $.ajax({
                type: "Post",
                url: UrlAutoComplete,
                data: { mykey: extractLast(request.term) },
                ajaxasync: true,
                dataType: "json",
                ajaxasync: false,
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item.backOfficeCustomerID };
                    }))
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },

    });
    $("#txtCustomer").bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    })
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }
    /// Child customer autocomplete
    $("#txtchildCustomer").autocomplete({
        source: function (request, response) {
            var UrlAutoComplete = $('#hdAutoCompleteChild').val();
            $.ajax({
                type: "Post",
                url: UrlAutoComplete,
                data: { mykey: extractLast(request.term) },
                ajaxasync: true,
                dataType: "json",
                ajaxasync: false,
                success: function (data) {

                    response($.map(data, function (item) {
                        return { label: item.backOfficeCustomerID };
                    }))

                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },

    });
    $("#txtchildCustomer").bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    })
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }

    // AutoComplete advance search page
    $("#txtCustomerCreate").autocomplete({
        source: function (request, response) {
            var UrlAutoComplete = $('#hdAutoComplete').val();
            $.ajax({

                type: "Post",
                url: UrlAutoComplete,
                data: { mykey: extractLast(request.term) },
                ajaxasync: true,

                dataType: "json",

                //
                ajaxasync: false,
                success: function (data) {
                    response(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            var terms = split(this.value);
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(", ");
            return false;
        }
    });
    $("#txtCustomerCreate").bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    })
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }

    /// AutoComplete Document Name From Libary


    $("#txtAutofromlibary").autocomplete({
        source: function (request, response) {
            var hdAutoCompleteDocument = $('#hdAutoCompleteDocument').val();
            $.ajax({

                type: "Post",
                url: hdAutoCompleteDocument,
                data: { mykey: extractLast(request.term) },
                ajaxasync: true,

                dataType: "json",

                //
                ajaxasync: false,
                success: function (data) {
                    response(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },

    });
    $("#txtAutofromlibary").bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    })
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }


}

function AddForuncheck(checkbox, BackOfficeCustomerID) {

    if ($(checkbox).is(':checked')) {
        var single = $("#hdUncheckList").val()
        var delList = $("#hdUncheckList").val().split(',');
        var index = delList.indexOf(BackOfficeCustomerID.toString());
        if (index > -1) {
            delList.splice(index, 1);
        }
        var bbb = delList.toString().replace(/,/g, ',')
        $("#hdUncheckList").val(bbb);
    }
    else {

        var hdUncheckList = $("#hdUncheckList").val();

        if (hdUncheckList != "") {
            hdUncheckList = hdUncheckList + ',' + BackOfficeCustomerID;
        } else
            hdUncheckList = BackOfficeCustomerID;

        $("#hdUncheckList").val(hdUncheckList);
    }
}
function AddForcheck(checkbox, BackOfficeCustomerID) {
    if ($(checkbox).is(':checked')) {
        var hdcheckedList = $("#hdcheckedList").val();

        if (hdcheckedList != "") {
            hdcheckedList = hdcheckedList + ',' + BackOfficeCustomerID;
        } else
            hdcheckedList = BackOfficeCustomerID;

        $("#hdcheckedList").val(hdcheckedList);
    }
    else {
        var single = $("#hdcheckedList").val()
        var delList = $("#hdcheckedList").val().split(',');
        var index = delList.indexOf(BackOfficeCustomerID.toString());
        if (index > -1) {
            delList.splice(index, 1);
        }
        var bbb = delList.toString().replace(/,/g, ',')
        $("#hdcheckedList").val(bbb);
    }
}
function binddataonchange() {
    var BackofficeCustomerID = $('#txtCustomer').val();
    if (BackofficeCustomerID == "") {

        return false;
    }

    $('#txtchildCustomer').val('');

    var currentName = '';
    var currentBackoffice
    $("#ui-id-1 li").each(function (index, value) {

        currentName = $(value).text()

        if (currentName.toUpperCase().indexOf(BackofficeCustomerID.toUpperCase()) > -1) {

            currentBackoffice = BackofficeCustomerID;
        } else {

        }

    });
    if (currentBackoffice == BackofficeCustomerID) {

        $("div.alert-dangerparent").hide();
    }
    else {
        $("div.alert-dangerparent").show();
        $("div.alert-dangerchild").hide();
        $('#txtCustomer').val('');
        $(".loader-wrapper").removeClass("show1");
        return false;
    }
    $("div.alert-dangerchild").hide();
    $(".loader-wrapper").addClass("show1");
    var hdGetChildJoiningAccountData = $('#hdGetChildJoiningAccountData').val();
    $.ajax({
        type: "Post",
        url: hdGetChildJoiningAccountData,
        data: { BackofficeCustomerID: BackofficeCustomerID },
        dataType: "html",
        success: function (result) {

            $("div.btnparentjoinaccount").show();
            $("div.btnchildjoinaccount").hide();

            $('#JoinAccountList').children().remove();
            $('#JoinAccountList').html(result);
            $('#tblJoinAccount').DataTable({


                language: {
                    "search": '<i class="fa fa-search"></i>',
                    searchPlaceholder: "Search records",
                    paginate: {
                        next: '<i class="fa fa-angle-right"></i>',
                        previous: '<i class="fa fa-angle-left"></i>'
                    }
                },
                "searching": true,
                "lengthChange": false,
                "paging": true,
                "info": true,
                "order": []



            });
            $("div.alert-dangerparent").hide();
            $(".loader-wrapper").removeClass("show1");

            setTimeout(function () {

            }, 1000);


        },
        error: function (response) {
            $("div.alert-dangerparent").show();
            $('#txtCustomer').val('');
            $(".loader-wrapper").removeClass("show1");
        }
    });
}
function bindparentonchange() {
    var BackofficeCustomerID = $('#txtchildCustomer').val();
    if (BackofficeCustomerID == "") {

        return false;
    }

    $('#txtCustomer').val('');
    var currentName = '';
    var currentBackoffice
    $("#ui-id-2 li").each(function (index, value) {

        currentName = $(value).text()

        if (currentName.toUpperCase().indexOf(BackofficeCustomerID.toUpperCase()) > -1) {

            currentBackoffice = BackofficeCustomerID;
        } else {

        }

    });
    if (currentBackoffice == BackofficeCustomerID) {

        $("div.alert-dangerchild").hide();
    }
    else {
        $("div.alert-dangerchild").show();
        $("div.alert-dangerparent").hide();
        $('#txtchildCustomer').val('');
        $(".loader-wrapper").removeClass("show1");
        return false;
    }
    $("div.alert-dangerparent").hide();
    $(".loader-wrapper").addClass("show1");
    var hdGetParentJointInChild = $('#hdGetParentJointInChild').val();
    $.ajax({
        type: "Post",
        url: hdGetParentJointInChild,
        data: { BackofficeCustomerID: BackofficeCustomerID },
        dataType: "html",
        success: function (result) {
            $("div.btnparentjoinaccount").hide();
            $("div.btnchildjoinaccount").show();

            $('#JoinAccountList').children().remove();
            $('#JoinAccountList').html(result);
            $('#tblJoinAccount').DataTable({


                language: {
                    "search": '<i class="fa fa-search"></i>',
                    searchPlaceholder: "Search records",
                    paginate: {
                        next: '<i class="fa fa-angle-right"></i>',
                        previous: '<i class="fa fa-angle-left"></i>'
                    }
                },
                "searching": true,
                "lengthChange": false,
                "paging": true,
                "info": true,
                "order": []



            });
            $(".loader-wrapper").removeClass("show1");

            setTimeout(function () {

            }, 1000);


        },
        error: function (response) {
            $("div.alert-dangerchild").show();
            $('#txtchildCustomer').val('');
            $(".loader-wrapper").removeClass("show1");
        }
    });
}
function joinaccount() {
    var checkeds = '';
    var uncheckeds = '';
    var BackofficeCustomerID = $('#txtCustomer').val();

    if (BackofficeCustomerID == "") {
        $('#txtCustomer').css('border-color', 'red');
        $('#txtCustomer').focus();
        return false;
    }

    $(".loader-wrapper").addClass("show1");
    $(".alert-dangerbillingType").hide();
    checkeds = $("#hdcheckedList").val();
    uncheckeds = $("#hdUncheckList").val();

    var hdJoiningAccount = $('#hdJoiningAccount').val();
    $.ajax({
        type: "Post",
        url: hdJoiningAccount,
        data: { BackofficeCustomerID: BackofficeCustomerID, checkeds: checkeds, uncheckeds: uncheckeds },
        success: function (result) {
            if (result.success == false) {
                $(".alert-dangerbillingType").html(result.message);
                $(".alert-dangerbillingType").show();
                $(".loader-wrapper").removeClass("show1");
                return false;
            }
            $("div.updatemsg").show();
            setTimeout(function () {
                $("#hdcheckedList").val('');
                $("#hdUncheckList").val('');
                $("div.updatemsg").hide();
                $("div.alert-danger").hide();
                binddataonsavechild();
            }, 1000);
        },
        error: function (response) {

        }
    });

}
function joinchildanotherparent() {
    var checkeds = '';
    var uncheckeds = '';

    var BackofficeCustomerID = $('#txtchildCustomer').val();

    if (BackofficeCustomerID == "") {
        $('#txtchildCustomer').css('border-color', 'red');
        $('#txtchildCustomer').focus();
        return false;
    }

    $(".loader-wrapper").addClass("show1");

    checkeds = $("#hdcheckedList").val();
    uncheckeds = $("#hdUncheckList").val();

    var hdjoinchildanotherparent = $('#hdjoinchildanotherparent').val();
    $.ajax({
        type: "Post",
        url: hdjoinchildanotherparent,
        data: { BackofficeCustomerID: BackofficeCustomerID, checkeds: checkeds, uncheckeds: uncheckeds },
        success: function (result) {
            $("div.updatemsg").show();
            setTimeout(function () {
                $("#hdcheckedList").val('');
                $("#hdUncheckList").val('');
                $("div.updatemsg").hide();
                $("div.alert-danger").hide();
                bindparentonchange();

                //var url = $(location).attr("href");
                //window.location.href = url;
            }, 1000);


        },
        error: function (response) {

        }
    });

}
function onLoadbinddata() {
    var values = "";

    $("#tblJoinAccount TBODY TR").each(function (index, value) {
        var row = $(this);

        var checkstatus = row.closest('tr').find('.chkRow').prop('checked');
        if (checkstatus == true) {
            values += (!values ? row.find(".chkRow").val() : (',' + row.find(".chkRow").val()));
            $("#hdcheckedList").val(values);
            $("div.btnparentjoinaccount").show();
        }

    });


}
function binddataonsavechild() {
    var BackofficeCustomerID = $('#txtCustomer').val();
    if (BackofficeCustomerID == "") {

        return false;
    }

    $("div.alert-dangerchild").hide();
    $(".loader-wrapper").addClass("show1");
    var hdGetChildJoiningAccountData = $('#hdGetChildJoiningAccountData').val();
    $.ajax({
        type: "Post",
        url: hdGetChildJoiningAccountData,
        data: { BackofficeCustomerID: BackofficeCustomerID },
        dataType: "html",
        success: function (result) {

            $("div.btnparentjoinaccount").show();
            $("div.btnchildjoinaccount").hide();

            $('#JoinAccountList').children().remove();
            $('#JoinAccountList').html(result);
            $('#tblJoinAccount').DataTable({


                language: {
                    "search": '<i class="fa fa-search"></i>',
                    searchPlaceholder: "Search records",
                    paginate: {
                        next: '<i class="fa fa-angle-right"></i>',
                        previous: '<i class="fa fa-angle-left"></i>'
                    }
                },
                "searching": true,
                "lengthChange": false,
                "paging": true,
                "info": true,
                "order": []



            });
            $("div.alert-dangerparent").hide();
            $(".loader-wrapper").removeClass("show1");

            setTimeout(function () {

            }, 1000);


        },
        error: function (response) {
            $("div.alert-dangerparent").show();
            $('#txtCustomer').val('');
            $(".loader-wrapper").removeClass("show1");
        }
    });
}
function clearControls() {
    $("div.alert-dangerparent, div.alert-dangerchild, div.alert-dangerbillingType").hide();
    $('#tblJoinAccount').DataTable().clear().draw();
    $("#txtCustomer, #txtchildCustomer, input[type='search']").val('');
}
function Confirm(elem) {
    var bckOfcId = $(elem).parent().parent().find(".joinAccountToClear").val();
    if (bckOfcId != "") {
        
        $.ajax({
            type: "Post",
            url: '/customers/getcustomername',
            data: { BackOfficeCustomerID: bckOfcId },
            success: function (result) {
                $("#ClearJoinConfirm").modal("show");
                $("#btnConfirm").attr("data-id", bckOfcId);
                $('#lblcustomername').text();
                $("#joinAccount-message").html('Account ' + bckOfcId + ' (' + result + ') will be UN-JOINED');
            },
            error: function (response) {
                alert('No account found')
            }
        });
    }
}
function ClearJoin(elem) {

    $("div.updatemsg").hide();
    $(".alert-dangerchild").hide();
    $(".loader-wrapper").addClass("show1");
    $("#ClearJoinConfirm").modal("hide");
    var backOfcId = $(elem).attr("data-id");
    $.ajax({
        url: '/JoinAccounts/CancelJoin?BackofficeCustomerID=' + backOfcId,
        method: "POST",
        success: function (data) {

            if (data) {

                $("div.updatemsg").show();
                setTimeout(function () {
                    location.reload();
                }, 3000);

                $(".loader-wrapper").removeClass("show1");
                $(".joinAccountToClear").val(" ");

            }
            else {

                $(".alert-dangerchild").show();

                $([document.documentElement, document.body]).animate({
                    scrollTop: 0
                }, 500);
                $(".loader-wrapper").removeClass("show1");
            }
        }

    });
}