﻿var notificationData;
$(document).ready(function () {
   
       var today = new Date();
        var currMonth = today.getMonth();
        var currYear = today.getFullYear();
        var startDate = new Date(currYear, currMonth, 1);


        var EndDates = new Date();
        var currMonths = EndDates.getMonth();
        var currYears = EndDates.getFullYear();
        //var EndDate = new Date(currYears, currMonths, -1);
        var EndDate = new Date();


        var date = new Date();
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        var d = date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
        $('#txtStartDate').datepicker({
            format: 'mm/dd/yyyy',
            startDate: '-3d',
            defaultViewDate: 'today'

        });


        $('#txtEndDate').datepicker({
            format: 'mm/dd/yyyy',
            startDate: '-3d'
        });

        $('#txtStartDate').datepicker('setDate', startDate);
        $('#txtEndDate').datepicker('setDate', EndDate);
        load_settleReport();
        SearchText();
    });

    function load_settleReport() {
        var today = new Date();
        var currMonth = today.getMonth();
        var currYear = today.getFullYear();
        var startDate = new Date(currYear, currMonth, 1);


        var EndDates = new Date();
        var currMonths = EndDates.getMonth();
        var currYears = EndDates.getFullYear();
        //var EndDate = new Date(currYears, currMonths, -1);
        var EndDate = new Date();


        var date = new Date();
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        var d = date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
        var StartDate = $('#txtStartDate').val();
        var EndDate = $('#txtEndDate').val();
        if (StartDate == '') {

            swal({

                text: "Please select start date.",
                icon: "error",

            })
                .then((ok) => {
                    if (ok) {


                    }
                });
            return false;
        }

        if (EndDate == '') {

            swal({

                text: "Please select end date.",
                icon: "error",

            })
                .then((ok) => {
                    if (ok) {


                    }
                });
            return false;
        }
       
        load_data(0, 10);
       

    $('#pagelist').change(function () {
        load_data();
        var page_number = parseInt($('#pagelist').val());

        var table = $('#tblNotification').dataTable();

        table.fnPageChange(page_number - 1);

    });

        function load_data(start, length) {
            $('#tblNotification').DataTable().destroy();
        var dataTable = $("#tblNotification").dataTable({
            "serverSide": true,
            "order": [[5, "desc"]],
            language: {
                "search": '<i class="fa fa-search"></i>',
                searchPlaceholder: "Search records",
                paginate: {
                    next: 'Next &raquo;',
                    previous: '&laquo; Previous'
                },
                "processing": '<div class="loader-wrapper show1"><div class= "loader" ></div></div>'
            },
            "processing": true,
            "pagingType": "input",
            "filter": true,
            "orderMulti": false,
            "destroy": true,
            "lengthChange": false,
            "retrieve": true,
            "ajax": {
                url: '/Reports/GetNotificationDetails',
                method: "POST",
                data: { StartDate: StartDate, EndDate: EndDate },
            },
            "drawCallback": function (settings) {
                $('#pagelist').val('');
                notificationData = settings.json.data;
            },
            "columns": [
                { "data": "to", "name": "to", "width": "250px" }
                , { "data": "subject", "name": "subject", "width": "350px" }
                , {
                    "data": "body", "name": "body", "width": "100px", mRender: function (data, type, full, rowIndex) {
                        return '<a href="#" data-toggle="modal" data-target="#notificationEmailBody" onclick="openNotificationBody(\'' + rowIndex.row + '\');return false;"><img style="height:25px; width:20px; cursor:pointer;" src="/img_new/messageicon.png" /></a>';
                    }
                }
                , { "data": "isSent", "name": "isSent", "width": "150px" }
                , {
                    "data": "createdOn", "name": "createdOn", "width": "200px", mRender: function (data, type, full) {
                        var date = new Date(full.createdOn);
                        var day = date.getDate();
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();
                        var hour = date.getHours();
                        var min = date.getMinutes();
                        var sec = date.getSeconds();;
                        var format = "AM";
                        if (day < 10) {
                            day = "0" + day;
                        }
                        if (month < 10) {
                            month = "0" + month;
                        }
                        if (sec < 10) {
                            sec = "0" + sec;
                        }
                        if (hour > 11) { format = "PM"; }
                        if (hour > 12) { hour = hour - 12; }
                        if (hour == 0) { hour = 12; }
                        if (min < 10) { min = "0" + min; }
                        var date = month + "/" + day + "/" + year + " " + hour + ":" + min + ":" + sec + " " + format;
                        return date;
                    }
                }
                , { "data": "error", "name": "error", sortable: false }
                

            ]
        });
    }
    }


function SearchText() {



    // AutoComplete advance search page
    $("#txtCustomerCreate").autocomplete({
        source: function (request, response) {
            var UrlAutoComplete = $('#hdAutoComplete').val();
            $.ajax({

                type: "Post",
                url: UrlAutoComplete,
                data: { mykey: extractLast(request.term) },
                ajaxasync: true,

                dataType: "json",

                //
                ajaxasync: false,
                success: function (data) {
                    response(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            var terms = split(this.value);
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(", ");
            return false;
        }
    });
    $("#txtCustomerCreate").bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    })
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }

    /// AutoComplete Document Name From Libary


    $("#txtAutofromlibary").autocomplete({
        source: function (request, response) {
            var hdAutoCompleteDocument = $('#hdAutoCompleteDocument').val();
            $.ajax({

                type: "Post",
                url: hdAutoCompleteDocument,
                data: { mykey: extractLast(request.term) },
                ajaxasync: true,

                dataType: "json",

                //
                ajaxasync: false,
                success: function (data) {
                    response(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },

    });
    $("#txtAutofromlibary").bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    })
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }

}

function openNotificationBody(index)
{
    $("#notificationEmailBody .modal-body").html(notificationData[index].body);
}

