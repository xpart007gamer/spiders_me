﻿$(document).ready(function () {

    //var dates = new Date();
    //var today = new Date(dates.getFullYear(), dates.getMonth()-1, dates.getDate());
    //var EndDate = new Date(dates.getFullYear(), dates.getMonth(), dates.getDate());
    var today = new Date();
    var currMonth = today.getMonth();
    var currYear = today.getFullYear();
    var startDate = new Date(currYear, currMonth, 1);


    var EndDates = new Date();
    var currMonths = EndDates.getMonth();
    var currYears = EndDates.getFullYear();
    //var EndDate = new Date(currYears, currMonths, -1);
    var EndDate = new Date();


    var date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    var d = date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;

    $('#txtStartDate').datepicker({
        format: 'mm/dd/yyyy',
        startDate: '-3d',
        defaultViewDate: 'today'

    });


    $('#txtEndDate').datepicker({
        format: 'mm/dd/yyyy',
        startDate: '-3d'
    });

    $('#txtStartDate').datepicker('setDate', startDate);
    $('#txtEndDate').datepicker('setDate', EndDate);


    var StartDate = $('#txtStartDate').val();
    var EndDate = $('#txtEndDate').val();
    if (StartDate == '') {

        swal({

            text: "Please select start date.",
            icon: "error",

        })
            .then((ok) => {
                if (ok) {


                }
            });
        return false;
    }

    if (EndDate == '') {

        swal({

            text: "Please select end date.",
            icon: "error",

        })
            .then((ok) => {
                if (ok) {


                }
            });
        return false;
    }

    if ($.fn.DataTable.isDataTable('#tblpaymentreport')) {
        $('#tblpaymentreport').DataTable().destroy();
    }

    $(".loader-wrapper").addClass("show1");

    var hdGetPaymentReportDetails = $('#hdGetPaymentReportDetails').val();
    $.ajax({
        type: "Post",
        url: hdGetPaymentReportDetails,
        data: { StartDate: StartDate, EndDate: EndDate },
        dataType: "json",
        success: function (response) {
           
            var count = Object.keys(response).length;
            $('#tblpaymentreport').dataTable({

                data: response,
                "columns": [


                    //{
                    //    "data": "paymentDate",
                    //    "render": function (data) {
                    //        if (data == null) {
                    //            return '<label></label>';
                    //        }
                    //        else {

                    //            return moment(data).format('MM/DD/YYYY');
                    //            //var date = new Date(data);
                    //            //var month = date.getMonth() + 1;
                    //            //return (month.toString().length > 1 ? month : "0" + month) + "/" + date.getDate() + "/" + date.getFullYear();
                    //        }
                    //    }
                    //},
                    {
                        "data": "settledDate",
                        "render": function (data) {
                            if (data == null) {
                                return '<label></label>';
                            }
                            else {

                                return moment(data).format('MM/DD/YYYY');
                            }
                        }
                    }
                    , { "data": "paymentTransId", "name": "paymentTransId", "autoWidth": true }
                    , { "data": "serviceAccountNumber", "name": "serviceAccountNumber", "autoWidth": true }
                    , { "data": "companyServiceTypeSource", "name": "companyServiceTypeSource", "autoWidth": true }
                    , { "data": "name", "name": "name", "autoWidth": true }
                    , { "data": "amount", "name": "amount", "autoWidth": true }




                ],
                "dom": 'Bfrtip',
                "buttons": [

                    {
                        extend: 'pdf', footer: true,
                        customize: function (doc) {
                            doc['footer'] = (function (page, pages) {
                                return {
                                    columns: [
                                        {

                                            alignment: 'left',
                                            text: [

                                                { text: d.toString() },
                                                '                                                                                                                                                                           ',
                                                { text: page.toString() },
                                                ' of ',
                                                { text: pages.toString() }
                                            ]

                                        }],

                                    margin: [10, 0]
                                }
                            });


                        },

                        title: 'SETTLED START :' + ' ' + StartDate + ' END : ' + ' ' + EndDate + '',
                        className: 'fa fa-download btn-green fld-tb',
                        text: '<i class=""></i> Download',
                        pageSize: 'LETTER'

                    }

                ],

                language: {
                    "search": '<i class="fa fa-search"></i>',
                    searchPlaceholder: "Search records",
                    paginate: {
                        next: 'Next &raquo;',
                        previous: '&laquo; Previous'
                    }
                },
                "pagingType": "input",
                "lengthChange": false,
                "paging": true,
                "info": true,
                "iDisplayLength": 10,
                "bDestroy": true,
                "footerCallback": function (row, data, start, end, display) {
                    var api = this.api(), data;

                    var intVal = function (i) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                                i : 0;
                    };



                    $(api.column(1).footer()).html(
                        ' ' + count + ' '
                    );

                    total = api
                        .column(5)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    pageTotal = api
                        .column(5, { page: 'current' })
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    $(api.column(5).footer()).html(
                        ' ' + total.toFixed(2) + ' '
                    );
                    //$(api.column(5).footer()).html(
                    //    ' ' + pageTotal.toFixed(2) + ' '
                    //);


                }


            });

            $(".loader-wrapper").removeClass("show1");

            setTimeout(function () {
                $(".loader-wrapper").removeClass("show1");
            }, 10000);
        },
        error: function (response) {
            $(".loader-wrapper").removeClass("show1");
        }
    });




    SearchText();

});


function SearchText() {



    // AutoComplete advance search page
    $("#txtCustomerCreate").autocomplete({
        source: function (request, response) {
            var UrlAutoComplete = $('#hdAutoComplete').val();
            $.ajax({

                type: "Post",
                url: UrlAutoComplete,
                data: { mykey: extractLast(request.term) },
                ajaxasync: true,

                dataType: "json",

                //
                ajaxasync: false,
                success: function (data) {
                    response(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            var terms = split(this.value);
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(", ");
            return false;
        }
    });
    $("#txtCustomerCreate").bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    })
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }

    /// AutoComplete Document Name From Libary


    $("#txtAutofromlibary").autocomplete({
        source: function (request, response) {
            var hdAutoCompleteDocument = $('#hdAutoCompleteDocument').val();
            $.ajax({

                type: "Post",
                url: hdAutoCompleteDocument,
                data: { mykey: extractLast(request.term) },
                ajaxasync: true,

                dataType: "json",

                //
                ajaxasync: false,
                success: function (data) {
                    response(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },

    });
    $("#txtAutofromlibary").bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    })
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }

}



function SearchSettlePayment() {

    var StartDate = $('#txtStartDate').val();
    var EndDate = $('#txtEndDate').val();

    var date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    var d = date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;

    if (StartDate == '') {

        swal({

            text: "Please select start date.",
            icon: "error",

        })
            .then((ok) => {
                if (ok) {


                }
            });
        return false;
    }

    if (EndDate == '') {

        swal({

            text: "Please select end date.",
            icon: "error",

        })
            .then((ok) => {
                if (ok) {


                }
            });
        return false;
    }
    if ($.fn.DataTable.isDataTable('#tblpaymentreport')) {
        $('#tblpaymentreport').DataTable().destroy();
    }

    $(".loader-wrapper").addClass("show1");

    var hdGetPaymentReportDetails = $('#hdGetPaymentReportDetails').val();
    $.ajax({
        type: "Post",
        url: hdGetPaymentReportDetails,
        data: { StartDate: StartDate, EndDate: EndDate },
        dataType: "json",
        success: function (response) {

            var count = Object.keys(response).length;

            $('#tblpaymentreport').dataTable({

                data: response,

                "columns": [


                    //{
                    //    "data": "paymentDate",
                    //    "render": function (data) {
                    //        if (data == null) {
                    //            return '<label></label>';
                    //        }
                    //        else {
                    //            return moment(data).format('MM/DD/YYYY');
                    //            //var date = new Date(data);
                    //            //var month = date.getMonth() + 1;
                    //            //return (month.toString().length > 1 ? month : "0" + month) + "/" + date.getDate() + "/" + date.getFullYear();
                    //        }
                    //    }
                    //},
                    {
                        "data": "settledDate",
                        "render": function (data) {
                            
                            if (data == null) {
                                return '<label></label>';
                            }
                            else {
                                return moment(data).format('MM/DD/YYYY');
                            }
                        }
                    }
                    , { "data": "paymentTransId", "name": "paymentTransId", "autoWidth": true }
                    , { "data": "serviceAccountNumber", "name": "serviceAccountNumber", "autoWidth": true }
                    , { "data": "companyServiceTypeSource", "name": "companyServiceTypeSource", "autoWidth": true }
                    , { "data": "name", "name": "name", "autoWidth": true }
                    , { "data": "amount", "name": "amount", "autoWidth": true }




                ],
                "dom": 'Bfrtip',
                "buttons": [

                    {
                        extend: 'pdf', footer: true,
                        customize: function (doc) {
                            doc['footer'] = (function (page, pages) {
                                return {
                                    columns: [
                                        {

                                            alignment: 'left',
                                            text: [

                                                { text: d.toString() },
                                                '                                                                                                                                                                           ',
                                                { text: page.toString() },
                                                ' of ',
                                                { text: pages.toString() }
                                            ]

                                        }],

                                    margin: [10, 0]
                                }
                            });


                        },

                        title: 'SETTLED START DATE :' + ' ' + StartDate + ' END DATE : ' + ' ' + EndDate + '',
                        className: 'fa fa-download btn-green fld-tb',
                        text: '<i class=""></i> Download',
                        pageSize: 'LETTER'

                    }

                ],

                language: {
                    "search": '<i class="fa fa-search"></i>',
                    searchPlaceholder: "Search records",
                    paginate: {
                        next: '<i class="fa fa-angle-right"></i>',
                        previous: '<i class="fa fa-angle-left"></i>'
                    }
                },
                "lengthChange": false,
                "paging": true,
                "info": true,
                "iDisplayLength": 10,
                "bDestroy": true,
                "footerCallback": function (row, data, start, end, display) {
                    var api = this.api(), data;

                    var intVal = function (i) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                                i : 0;
                    };



                    $(api.column(1).footer()).html(
                        ' ' + count + ' '
                    );

                    total = api
                        .column(5)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    pageTotal = api
                        .column(5, { page: 'current' })
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    $(api.column(5).footer()).html(
                        ' ' + total.toFixed(2) + ' '
                    );



                }

            });

            $(".loader-wrapper").removeClass("show1");

            setTimeout(function () {
                $(".loader-wrapper").removeClass("show1");
            }, 10000);
        },
        error: function (response) {
            $(".loader-wrapper").removeClass("show1");
        }
    });

}