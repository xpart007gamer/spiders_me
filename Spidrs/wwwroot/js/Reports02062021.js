﻿
$(document).ready(function () {
    GetPaymentActivityReport();
    //$(".loader-wrapper").removeClass("show1");
    SearchText();
});

function SearchText() {



    // AutoComplete advance search page
    $("#txtCustomerCreate").autocomplete({
        source: function (request, response) {
            var UrlAutoComplete = $('#hdAutoComplete').val();
            $.ajax({

                type: "Post",
                url: UrlAutoComplete,
                data: { mykey: extractLast(request.term) },
                ajaxasync: true,

                dataType: "json",

                //
                ajaxasync: false,
                success: function (data) {
                    response(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            var terms = split(this.value);
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(", ");
            return false;
        }
    });
    $("#txtCustomerCreate").bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    })
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }

    /// AutoComplete Document Name From Libary

    
    $("#txtAutofromlibary").autocomplete({
        source: function (request, response) {
            var hdAutoCompleteDocument = $('#hdAutoCompleteDocument').val();
            $.ajax({

                type: "Post",
                url: hdAutoCompleteDocument,
                data: { mykey: extractLast(request.term) },
                ajaxasync: true,

                dataType: "json",

                //
                ajaxasync: false,
                success: function (data) {
                    response(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },

    });
    $("#txtAutofromlibary").bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    })
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }

}
$("#ddlDaysRange").change(function () {
    GetPaymentActivityReport();
});
$("#ddlServiceType").change(function () {
    GetPaymentActivityReport();
});
$("#ddlCompany").change(function () {
    GetPaymentActivityReport();
});


function formatDate(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
}




function GetPaymentActivityReport() {
 
    $(".loader-wrapper").addClass("show1");
    var DateRange = "";
    var ServiceTypeID = "";
    var Company = "";
    DateRange = $('#ddlDaysRange').val();
    ServiceTypeID = $('#ddlServiceType').val();
    Company = $('#ddlCompany').val();

    var currentDate = new Date();
    //var day = currentDate.getDate()
    //var month = currentDate.getMonth() + 1
    //var year = currentDate.getFullYear()  
  


    var d = formatDate(currentDate);

    $('#tblpaymentactivity').dataTable().fnClearTable();
    $('#tblpaymentactivity').dataTable().fnDestroy();

    var hdGetPaymentActivityReport = $('#hdGetPaymentActivityReport').val();
    $.ajax({
        type: "Post",
        url: hdGetPaymentActivityReport,
        data: { DateRange: DateRange, ServiceTypeID: ServiceTypeID, Company: Company },
        dataType: "json",
        success: function (response) {
            
            $('#tblpaymentactivity').dataTable({
                'columnDefs': [        
                    {
                        'searchable': false,
                        'targets': [0, 1,2,3, 4, 6, 7, 8]
                    },
                ],
                data: response,
                "columns": [
                   

                    {
                        "data": "paymentDate",
                        "render": function (data) {
                            if (data == null) {
                                return '<label></label>';
                            }
                            else {
                                return moment(data).format('MM/DD/YYYY');
                                //var date = new Date(data);
                                //var month = date.getMonth() + 1;
                                //return (month.toString().length > 1 ? month : "0" + month) + "/" + date.getDate() + "/" + date.getFullYear();
                            }
                        }
                    },

                    { "data": "serviceTypeName" },
                    { "data": "phone1" },
                    { "data": "serviceAccountNumber" },
                    { "data": "names" },
                    { "data": "serviceCity" },
                    { "data": "amount" },
                   
                    {
                        "data": "skipService",
                        "render": function (data) {
                            if (data == null) {
                                return '<label></label>';
                            }
                            else {
                                if (data != false) {
                                    return 'Skip';
                                }
                                else {
                                    return data = '';
                                }
                            }
                        }

                    },
                    { "data": "statusAbbreviation" }



                ],
                "dom": 'Bfrtip',
                "buttons": [
                    //{
                    //    extend: 'copy',
                    //    className: 'btn btn-dark rounded-0',
                    //    text: '<i class="far fa-copy"></i> Copy'
                    //},
                    //{
                    //    extend: 'excel',
                    //    className: 'btn btn-dark rounded-0',
                    //    text: '<i class="far fa-file-excel"></i> Excel'
                    //},
                    {
                        extend: 'pdf',
                        customize: function (doc) {
                           
                            $(".loader-wrapper").addClass("show1");

                            doc['footer'] = (function (page, pages) {
                                return {
                                    columns: [
                                        {

                                            alignment: 'left',
                                            text: [

                                                { text: d.toString() },
                                            '                                                                                                                                                                           ',
                                                { text: page.toString() },
                                                ' of ',
                                                { text: pages.toString() }
                                            ]

                                        }],

                                    margin: [10, 0]
                                }
                            });

                            if (window) {
                                
                                $(".loader-wrapper").removeClass("show1");
                            }
                        },
                        title: 'PAYMENT ACTIVITY REPORT' + ' ' + DateRange + ' Days',
                        className: 'fa fa-download btn-green fld-tb',
                        text: '<i class=""></i> Download',
                        pageSize:'LETTER'
                        
                        //  messageBottom: d
                        // customize: function (doc) {
                        //    doc.content[2].alignment = 'right';
                        //}
                    }
                    //{
                    //    extend: 'csv',
                    //    className: 'btn btn-dark rounded-0',
                    //    text: '<i class="fas fa-file-csv"></i> CSV'
                    //},
                    //{
                    //    extend: 'print',
                    //    className: 'btn btn-dark rounded-0',
                    //    text: '<i class="fas fa-print"></i> Print'
                    //}
                ],

                language: {
                    "search": '<i class="fa fa-search"></i>',
                    searchPlaceholder: "Search records",
                    paginate: {
                        next: '<i class="fa fa-angle-right"></i>',
                        previous: '<i class="fa fa-angle-left"></i>'
                    }
                },
                "lengthChange": false,
                "paging": true,
                "info": true,
                "iDisplayLength": 10,
                "bDestroy": true
             

            });

            $(".loader-wrapper").removeClass("show1");

            setTimeout(function () {
                $(".loader-wrapper").removeClass("show1");
            }, 10000);
        },
        error: function (response) {
            $(".loader-wrapper").removeClass("show1");
        }
    });
}