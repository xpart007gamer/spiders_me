﻿//Add dyanamic row
$(document).on('click', '#btnAddNewRow', function () {
   
    var firstRow = $(this).closest('table').find('tr').eq(1);
    $('#hdnHtml').html('');
    $('#hdnHtml').html($(firstRow).html());
    $('#hdnHtml').html($('#hdnHtml').html().replace('WebsitePath', 'WebsitePath' + makeid(5) + ''));
    $('#hdnHtml').html($('#hdnHtml').html().replace('fileupload', 'fileupload' + makeid(5) + ''));
    $('#hdnHtml').html($('#hdnHtml').html().replace('btnDeleteNewRow', 'btnDeleteNewRow'));
    $('#btnDeleteNewRow').css('display','block');
    _dvPreviewId = makeid(5);
    $('#hdnHtml').html($('#hdnHtml').html().replace('dvPreview', 'dvPreview' + _dvPreviewId + ''));
    $('#dvPreview' + _dvPreviewId + '').children().remove();
    $('#hdnHtml').html($('#hdnHtml').html().replace('errmsg1', 'errmsg1' + makeid(5) + ''));
    $('#hdnHtml').html($('#hdnHtml').html().replace('errwebsite', 'errwebsite' + makeid(5) + ''));
    $(this).closest('tr').after('<tr>' + $('#hdnHtml').html() + '</tr>');
    $('#hdnHtml').html('');
});

 
//Generate rendom number  
function makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
///Save the Multiple data images and websiteurl
function Saveall() {
   
    var wesite = "";
    var file = "";
    var formData1 = new FormData();
    formData1.append("ID", 1);
    $("#Advertismenttable TBODY TR").each(function (index, value) {
        var row = $(this);
         file = row.find(".fileclass")[0].files[0];
         wesite = row.find(".WebsitePaths").val()
        var errorimg = row.find(".errorimg").attr('id')
        var errorweb = row.find(".errorweb").attr('id')
        if (wesite == "") {
            $("#" + errorweb).html("Please Select Website Path").show()
            event.preventDefault()
        }
        else if (file == "" || file == undefined) {
            event.stopPropagation();
            $("#" + errorimg).html("Please Select Image").show()
            event.preventDefault()
        }
        
        else {
            formData1.append(row.find(".fileclass")[0].files[0].name, row.find(".fileclass")[0].files[0]);
            formData1.append("Websites", row.find(".WebsitePaths").val());
            formData1.append("ordernums", row.find(".ordernumbers").val());
        }

        
    });
    if (wesite != "" && file != "" && file != undefined) {
        var url = $("#hdSaveMultipleImages").val();
        $.ajax({
            type: "Post",
            url: url,
            contentType: false,
            processData: false,
            data: formData1,
            success: function (result) {
                if (result == 1) {
                    $("div.success").show();
                    $("div.success").fadeIn(400).delay(1600).fadeOut(3000);
                    setTimeout(function () {
                        var url = $("#hdnCreateAdvertisement").val();
                        window.location.href = url;
                    }, 300);
                }
              
                //else {

                //    $("div.failure").show();
                //    $("div.failure").fadeIn(400).delay(1600).fadeOut(3000);
                //}
            },
            error: function (response) {

            }
        });
    }
}
$(document).on('click', '#btnDeleteNewRow', function () {
    var rowId = $(this).closest('tr');
    rowId.remove();
});

//file on change
$(document).on('change', '.fileclass', function (e) {
   
    fileupload = $(this).attr('id');
    errormsg = e.currentTarget.nextElementSibling.id;
    imageshowdiv = e.currentTarget.parentElement.nextElementSibling.id

    Checkduplicateimage();

    $("#" + imageshowdiv).html("");
    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
    if (regex.test($(this).val().toLowerCase())) {

        $("#" + imageshowdiv).show();
        //$("#dvPreview")[0].filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = $(this).val();
        if (typeof (FileReader) != "undefined") {
            $("#" + imageshowdiv).show();
            $("#" + imageshowdiv).append("<img  style='height: 80px; width: 180px;'/>");
            var reader = new FileReader();
            reader.onload = function (e) {
                $("#" + imageshowdiv + " img").attr("src", e.target.result);

                $("#" + errormsg).html("Please upload a valid image file.").hide();
            }
            reader.readAsDataURL($(this)[0].files[0]);
        }
        else {
            alert("This browser does not support FileReader.");

        }

    } else {

        $("#" + errormsg).html("Please upload a valid image file.").show();
        $("#" + fileupload).val('');
        $("#" + fileupload).text('');
    }

});
///validation check
$('#checkvalidation').click(function () {

    var WebsitePath = $('#WebsitePath').val();
    if (WebsitePath == "") {
        $("#errmsg").html("Please Select Website Path").show().fadeOut(3000);
        return false;
    }
    var ID = $('#ID').val();
    if (ID == 0 || ID == "" || ID == undefined) {
        var file1 = $("#fileupload")[0].files[0]

        if (file1 == "" || file1 == undefined) {
            $("#errmsg1").html("Please Select Image").show().fadeOut(3000);
            return false;
        }
    }
});

////Check the image duplicate
function Checkduplicateimage() {
    
    var file1 = $("#" + fileupload + "")[0].files[0]
    if (file1 == "" || file1 == undefined) {
        $("#" + errormsg + "").html("Please Select Image").show().fadeOut(3000);
        return false;
    }
    var formData = new FormData();
    formData.append("checkimage", file1);
    var url = $('#hdnCheckexistimage').val();
    $.ajax({
        type: "Post",
        url: url,
        contentType: false,
        processData: false,
        data: formData,
        success: function (result) {
            if (result > 0) {
                $("#" + errormsg + "").html("This image already exist.").show();
                $("#" + fileupload + "").val('');
                $("#" + fileupload + "").text('');
                $("#" + imageshowdiv + "").html("");
                return false;
            }
        }
    });
}



$(document).ready(function () {

    $(".loader-wrapper").removeClass("show2");
    GetCustomers(1);
    SearchText();
});

function SearchText() {

    

    // AutoComplete advance search page
    $("#txtCustomerCreate").autocomplete({
        source: function (request, response) {
            var UrlAutoComplete = $('#hdAutoComplete').val();
            $.ajax({

                type: "Post",
                url: UrlAutoComplete,
                data: { mykey: extractLast(request.term) },
                ajaxasync: true,

                dataType: "json",

                //
                ajaxasync: false,
                success: function (data) {
                    response(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            var terms = split(this.value);
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(", ");
            return false;
        }
    });
    $("#txtCustomerCreate").bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    })
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }

    /// AutoComplete Document Name From Libary

    
    $("#txtAutofromlibary").autocomplete({
        source: function (request, response) {
            var hdAutoCompleteDocument = $('#hdAutoCompleteDocument').val();
            $.ajax({

                type: "Post",
                url: hdAutoCompleteDocument,
                data: { mykey: extractLast(request.term) },
                ajaxasync: true,

                dataType: "json",

                //
                ajaxasync: false,
                success: function (data) {
                    response(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },

    });
    $("#txtAutofromlibary").bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    })
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }

}
///Zoom the images
$(document).on('hover', '.zoom', function (e) {
    $(".zoom").hover(function () {
        $(this).css("-ms-transform", "scale(1.1)");
        $(this).css("-webkit-transform", "scale(1.1)");
        $(this).css("transform", "scale(1.1)");
        $(this).find('div:first').show()

    }, function () {
        $(this).css("-ms-transform", "scale(1)");
        $(this).css("-webkit-transform", "scale(1)");
        $(this).css("transform", "scale(1)");
        $(this).find('div:first').hide()
    });
});

$(document).on('hover', '.zoom-sm', function (e) {
    $(".zoom-sm").hover(function () {
        $(this).css("-ms-transform", "scale(1.1)");
        $(this).css("-webkit-transform", "scale(1.1)");
        $(this).css("transform", "scale(1.1)");
        $(this).find('div:first').show()

    }, function () {
        $(this).css("-ms-transform", "scale(1)");
        $(this).css("-webkit-transform", "scale(1)");
        $(this).css("transform", "scale(1)");
        $(this).find('div:first').hide()
    });
});


////pagging chnage event
$("body").on("click", ".Pager .page", function () {
    GetCustomers(parseInt($(this).attr('page')));
});
function GetCustomers(pageIndex) {
    var url = $('#hdnCreateAdvertisement').val();
    $.ajax({
        type: "Get",
        url: url,
        data: { id: 0, pageIndex: pageIndex },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
};
function OnSuccess(response) {
    
   // var url = $('#hdnCreateAdvertisement').val();
   //var url = "/KichiSagaTrading.Com/Home/CreateAdvertisement"
    var url = "/Home/CreateAdvertisement"
    $('#result').html('');
    var row = '';
    var result = response.imageModels;
 
    $.each(result, function (key, value) {
        row += `<div class='zoom'>
                        <div style="margin-left: 145px; display:none" class="mdsd">
                            <a class="fa fa-pencil-square-o" aria-hidden="true" id="edit" href="`+ url + `/` + value.id + `"></a>
                            <a class="fa fa-trash" aria-hidden="true" id="delete" style="cursor: pointer;" onclick="confirm1(` + value.id + `);"></a>
                        </div>
                        <a><img src="` + value.imagePath + `" style="height:80px; width:180px ;"/></a>
                            <a>`+ value.websitePath +`</a>
                    </div>`
    });
    $('#result').append(row);
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: response.pageIndex,
        PageSize: response.pageSize,
        RecordCount: result[0].recordCount
    });
};

function confirm1(id) {
    $('#cuids').val(id);
    $('#myModal45656').modal('show');
}
var Delete = function () {
   
    var id = $("#cuids").val();
    var deleteurl = $('#hdndeleteimage').val();
    $.ajax({
        type: "Post",
        url: deleteurl,
        data: { id: id },
        success: function (result) {
            $('#myModal45656').modal('hide');
            $("div.amountfailure").show();
            $("div.amountfailure").fadeIn(300).delay(1500).fadeOut(4000);
            setTimeout(function () {
                var url = $("#hdnCreateAdvertisement").val();
                window.location.href = url;
            }, 300);
        }
    })
}

/////Update case Advertismnet
$(function () {
    $("#fileupload1222").change(function () {
        CheckduplicateimageU();
        $("#dvPreview12").html("");
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
        if (regex.test($(this).val().toLowerCase())) {

            $("#dvPreview12").show();
            // $("#dvPreview")[0].filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = $(this).val();
            if (typeof (FileReader) != "undefined") {
                $("#dvPreview12").show();
                $("#dvPreview12").append("<img  style='height: 80px; width: 180px;'/>");
                var reader = new FileReader();
                reader.onload = function (e) {
                    $("#dvPreview12 img").attr("src", e.target.result);
                    $("#errmsg121").html("Please upload a valid image file.").hide();
                }
                reader.readAsDataURL($(this)[0].files[0]);
            }
            else {
                alert("This browser does not support FileReader.");

            }

        } else {

            $("#errmsg121").html("Please upload a valid image file.").show();
            $("#fileupload1222").val('');
            $("#fileupload1222").text('');
        }
    });
});

////Check the image duplicate /////Update case Advertismnet
function CheckduplicateimageU() {
    var file1 = $("#fileupload1222")[0].files[0]
    if (file1 == "" || file1 == undefined) {
        $("#errmsg121").html("Please Select Image").show().fadeOut(3000);
        return false;
    }
    var formData = new FormData();
    formData.append("checkimage", file1);
    var url = $('#hdnCheckexistimage').val();
    $.ajax({
        type: "Post",
        url: url,
        contentType: false,
        processData: false,
        data: formData,
        success: function (result) {
            if (result > 0) {
                $("#errmsg121").html("This image already exist.").show();
                $("#fileupload1222").val('');
                $("#fileupload1222").text('');
                $("#dvPreview12").html("");
                return false;
            }
        }
    });
}

////validation
$(document).on('change', '.WebsitePaths', function (e) {
    WebsitePath = $(this).attr('id');
    errormsg = e.currentTarget.nextElementSibling.id;
    var $th = $(this);
    if (isValidUrl($th.val()) == 1) {
        $("#" + errormsg).html("").show();
        return false;
    }
    else if (isValidUrl($th.val()) == -1) {
        $("#" + WebsitePath).val('');
        $("#" + errormsg).html("Not a valid url!").show();
        return false;
    }
});
//Check the url
function isValidUrl(url) {

    var myVariable = url;
    if (/^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(myVariable)) {
        return 1;
    } else {
        return -1;
    }
}