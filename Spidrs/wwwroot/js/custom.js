$(document).ready(function () {
    $(".icon-bar").click(function () {
        $("body").toggleClass("toggled");
    });
    $(window).resize(function () {
        /*If browser resized, check width again */
        if ($(window).width() < 767) {
            $('body').addClass('toggled');
        } else { $('body').removeClass('toggled'); }
    });


    //sidebar sub menu show/hide
    $("sidebar ul li.in-nav").click(function () {
        $("sidebar ul li.in-nav").not(this).removeClass("open");
        $(this).toggleClass("open");
    });

    //text editor
    //$("#txtEditor").Editor();

})